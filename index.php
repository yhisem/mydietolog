<?php
define('DS', DIRECTORY_SEPARATOR);
$webRoot = dirname(__FILE__);

include_once("protected/globals.php");
mb_internal_encoding("UTF-8");

if ($_SERVER['HTTP_HOST'] != 'mydietolog.kiev.ua') {
    define('YII_DEBUG', true);
    defined('YII_TRACE_LEVEL') or define('YII_TRACE_LEVEL', 3);
    require_once($webRoot . '/../../yii/framework/yii.php');
    $config = $webRoot . '/protected/config/dev.php';
}
else {
    define('YII_DEBUG', false);
    require_once($webRoot . '/yii/framework/yiilite.php');
    $config = $webRoot . '/protected/config/production.php';
}
Yii::createWebApplication($config)->run();
