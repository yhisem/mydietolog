<?/**
 * @var $data array()
 * @var $index
 */?>
<li style="height: auto;margin:0 0 20px 0;">
    <div class="extra-wrap">
        <h6 class="p2"><?=$data['name']?> <?=CHtml::link('delete-feedback-'.$data['id'],
                array('/feedback/delete','id'=>$data['id']),array('style'=>'float:right;'))?></h6>
        <?=$data['content']?>
        <p style="font:normal 13px Arial;color:#9595c3;">Email - <?=$data['email']?></p>
        <p style="font:normal 13px Arial;color:#9595c3;">Phone - <?=$data['phone']?></p>
    </div>
    <hr />
</li>