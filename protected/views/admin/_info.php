<?/**
 * @var $data array()
 * @var $index
 */?>
<li style="height: auto;margin:0 0 20px 0;">
    <div class="extra-wrap">
        <h6 class="p2">
            <?=CHtml::link($data['title'].' - language : '.$data['language']
                ,array('/posts/update','id'=>$data['id']))?>
            <span style="width: 50px;display: inline-block"></span>
            <?=CHtml::link('delete-'.$data['id'],array('/infopage/delete','id'=>$data['id'],'lang'=>$data['language']))?>
            <span style="width: 50px;display: inline-block"></span>

            <?$arrayLang = array('uk','ru','en');?>
            <?foreach($arrayLang as $one):?>
                <?if($one != $data['language']):?>
                    <?=CHtml::link('create/update '.$data['id'].' lang '.$one,
                        array('/infopage/create','id'=>$data['id'],'lang'=>$one))?>
                <?endif;?>
            <?endforeach?>

            </h6>

        <?=Posts::smallContent($data['content'])?><br />
        <?=CHtml::link('Read More',array('/posts/view','id'=>$data['id']),array('class'=>'link-2'))?>
    </div>
</li>
<hr />