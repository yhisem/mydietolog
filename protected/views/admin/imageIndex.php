<?/**
 * @var $dataProvider CSqlDataProvider
 * @var $this AdminController
 */?>
<section id="content">
    <div class="content-bg">
        <div class="container_24">
            <div class="wrapper">
                <article class="grid_8">
                    <div class="indent bcolor-1 prev-indent-bot">
                        <?$this->renderPartial('//block/adminMenu');?>
                    </div>
                    <figure><a href="#"><img src="/images/bunner-1.jpg" alt="" /></a></figure>
                </article>
                <article class="grid_16">
                    <h3 class="margin-top">Спиок картинок</h3>
                    <div class="slider-wrapper">
                        <h3 class="margin-top"><?=CHtml::link('Создать картинку',array('/images/create'),
                                array('style'=>'font-size: 13px;float: right;line-height: 93px'))?></h3>
                        <ul id="slider" style="height: auto">
                            <?php if(!empty($dataProvider)): ?>
                                <?php
                                $this->widget('zii.widgets.CListView', array(
                                    'dataProvider'=>$dataProvider,
                                    'itemView'=>'_image',
                                    'htmlOptions'=>array(
                                        'class' => 'index-pagination'
                                    ),
                                    'ajaxUpdate'=>false,
                                    'template' => '{items}{pager}',
                                    'pager' => array(
                                        'firstPageLabel'=>'',
                                        'prevPageLabel'=>'<span class="a">Предыдущие</span>',
                                        'nextPageLabel'=>'<span class="a2">Далее</span>',
                                        'nextPageCssClass' => 'gogo',
                                        'previousPageCssClass' => 'empty',
                                        'lastPageLabel'=>'',
                                        'maxButtonCount'=>'4',
                                        'header'=>'',
                                        'htmlOptions' => array(
                                            'class'=>'pagination small'
                                        ),
                                    ),
                                ));
                                ?>
                            <?php endif ?>
                        </ul>
                    </div>
                </article>
            </div>
        </div>
    </div>
</section>