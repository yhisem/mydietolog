<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html>
<head>
    <link rel="stylesheet" href="/css/slider.css" type="text/css" media="screen"/>
    <style>
        a.back{
            position:absolute;
            width:150px;
            height:27px;
            outline:none;
            top:2px;
            right:0;
        }
        *{
            margin:0;
            padding:0;
        }
    </style>
</head>
<body>
<div id="content">

    <div id="menuWrapper" class="menuWrapper bg1">
        <ul class="menu" id="menu">
            <li class="bg1" style="background-position:0 0;">
                <a id="bg1" href="#">Our Passion</a>
                <ul class="sub1" style="background-position:0 0;">
                    <li><a href="#">Submenu 1</a></li>
                    <li><a href="#">Submenu 2</a></li>
                    <li><a href="#">Submenu 3</a></li>
                </ul>
            </li>
            <li class="bg1" style="background-position:-199px 0;">
                <a id="bg2" href="#">Our Brands</a>
                <ul class="sub2" style="background-position:-199px 0;">
                    <li><a href="#">Submenu 1</a></li>
                    <li><a href="#">Submenu 2</a></li>
                    <li><a href="#">Submenu 3</a></li>
                </ul>
            </li>
            <li class="bg1" style="background-position:-398px 0;">
                <a id="bg3" href="#">Our Brands</a>
                <ul class="sub3" style="background-position:-199px 0;">
                    <li><a href="#">Submenu 1</a></li>
                    <li><a href="#">Submenu 2</a></li>
                    <li><a href="#">Submenu 3</a></li>
                </ul>
            </li>
            <li class="bg1" style="background-position:-597px 0;">
                <a id="bg4" href="#">Our Brands</a>
                <ul class="sub4" style="background-position:-199px 0;">
                    <li><a href="#">Submenu 1</a></li>
                    <li><a href="#">Submenu 2</a></li>
                    <li><a href="#">Submenu 3</a></li>
                </ul>
            </li>
            <li class="last bg1" style="background-position:-796px 0;">
                <a id="bg5" href="#">Contact</a>
                <ul class="sub5" style="background-position:-199px 0;">
                    <li><a href="#">Submenu 1</a></li>
                    <li><a href="#">Submenu 2</a></li>
                    <li><a href="#">Submenu 3</a></li>
                </ul>
            </li>
        </ul>
    </div>
</div>

<script type="text/javascript" src="/js/jquery-1.11.0.min.js"></script>
<script type="text/javascript" src="/js/jquery-migrate-1.2.1.js"></script>
<script type="text/javascript" src="/js/jquery.bgpos.js"></script>

<script type="text/javascript">
    $(function() {

        var current = 0;

        var loaded  = 0;
        for(var i = 1; i <6; ++i)
            $('<img />').load(function(){
                ++loaded;
                if(loaded == 5){
                    $('#bg1,#bg2,#bg3,#bg4,#bg5').mouseover(function(e){

                        var $this = $(this);

                        if($this.parent().index() == current)
                            return;


                        var item = e.target.id;

                        if(item == 'bg1' || current == 4)
                            $('#menu .sub'+parseInt(current+1)).stop().animate({backgroundPosition:"(-199px 0)"},300,function(){
                                $(this).find('li').hide();
                            });
                        else
                            $('#menu .sub'+parseInt(current+1)).stop().animate({backgroundPosition:"(199px 0)"},300,function(){
                                $(this).find('li').hide();
                            });

                        if(item == 'bg1' || current == 4){

                            $('#menu > li').animate({backgroundPosition:"(-999px 0)"},0).removeClass('bg1 bg2 bg3 bg4 bg5').addClass(item);
                            move(1,item);
                        }
                        else{
                            $('#menu > li').animate({backgroundPosition:"(999px 0)"},0).removeClass('bg1 bg2 bg3 bg4 bg5').addClass(item);
                            move(0,item);
                        }

                        if(current == 4 && item == 'bg1'){
                            $('#menu .sub'+parseInt(current)).stop().animate({backgroundPosition:"(-199px 0)"},300);
                        }
                        if(current == 0 && item == 'bg5'){
                            $('#menu .sub'+parseInt(current+2)).stop().animate({backgroundPosition:"(199px 0)"},300);
                        }
                        current = $this.parent().index();
                        $('#menu .sub'+parseInt(current+1)).stop().animate({backgroundPosition:"(0 0)"},300,function(){
                            $(this).find('li').fadeIn();
                        });
                    });
                }
            }).attr('src', '/../images/slider/'+i+'.jpg');

        function move(dir,item){
            if(dir){
                $('#bg1').parent().stop().animate({backgroundPosition:"(0 0)"},200);
                $('#bg2').parent().stop().animate({backgroundPosition:"(-199px 0)"},300);
                $('#bg3').parent().stop().animate({backgroundPosition:"(-398px 0)"},400);
                $('#bg4').parent().stop().animate({backgroundPosition:"(-597px 0)"},500);
                $('#bg5').parent().stop().animate({backgroundPosition:"(-796px 0)"},600,function(){
                    $('#menuWrapper').removeClass('bg1 bg2 bg3 bg4 bg5').addClass(item);
                });
            }
            else{
                $('#bg1').parent().stop().animate({backgroundPosition:"(0 0)"},400,function(){
                    $('#menuWrapper').removeClass('bg1 bg2 bg3 bg4 bg5').addClass(item);
                });
                $('#bg2').parent().stop().animate({backgroundPosition:"(-199px 0)"},300);
                $('#bg3').parent().stop().animate({backgroundPosition:"(-398px 0)"},300);
                $('#bg4').parent().stop().animate({backgroundPosition:"(-597px 0)"},300);
                $('#bg5').parent().stop().animate({backgroundPosition:"(-796px 0)"},200);
            }
        }
    });
</script>
</body>
</html>