<div class="userInfo">
    <h4 class="border-bot3" style="font-size: 32px;margin: 0;"><?=t('main','Ваши персональные данные')?></h4>
    <div class="row">
        <label for="UserInfo_user">User</label>
        <input name="UserInfo[user]" id="UserInfo_user" type="text" maxlength="124">
    </div>
    <div class="row">
        <label for="UserInfo_sex">Sex</label>
        <select name="UserInfo[sex]" id="UserInfo_sex">
            <option value="male" selected="selected">Male</option>
            <option value="female">Female</option>
        </select>
    </div>
    <div class="row">
        <label for="UserInfo_birthday">Birthday</label>
        <input name="UserInfo[birthday]" id="UserInfo_birthday" type="text">
    </div>
    <div class="row">
        <label for="UserInfo_height">Height</label>
        <input name="UserInfo[height]" id="UserInfo_height" type="text">
    </div>
    <div class="row">
        <label for="UserInfo_weight">Weight</label>
        <input name="UserInfo[weight]" id="UserInfo_weight" type="text">
    </div>
    <div class="row">
        <button id="send-form">Send</button>
    </div>
</div>
<script>
    $(document).on('click','#send-form',function()
    {
        var $user = $('#UserInfo_user').val();
        var $sex = $('#UserInfo_sex').val();
        var $birthday = $('#UserInfo_birthday').val();
        var $height = $('#UserInfo_height').val();
        var $weight = $('#UserInfo_weight').val();
        $.ajax({
            url: '/site/info',
            data: {user:$user,sex:$sex,birthday:$birthday,height:$height,weight:$weight},
            type: "POST",
            dataType: 'json'
        }).done(function (data) {
            $('.userInfo').html(data.massage)
        }).error(function (error) {
            alert(error.messages)
        });
    });


</script>