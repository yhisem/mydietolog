<?
/**
 * @var $slider array()
 * @var $activeId
 */
$arrayPosition = array(0,239,428,617,806);
?>
<link rel="stylesheet" href="/css/slider.css" type="text/css" media="screen"/>
<style>
    a.back{
        position:absolute;
        width:150px;
        height:27px;
        outline:none;
        top:2px;
        right:0;
    }
    *{
        margin:0;
        padding:0;
    }
    <?foreach($slider as $key=>$one):?>
        .bg<?=($key+1)?>{
            background-image: url('/images/slider/<?=($key+1)?>.<?=$one['ext']?>');
        }
    <?endforeach;?>
</style>
<div id="content" style="margin: 0 auto;background-color: #fff;padding: 3px 0 0;">
    <div id="menuWrapper" class="menuWrapper bg1" style="margin: 0 auto">
        <ul class="menu" id="menu">
            <?foreach($slider as $key=>$one):?>
                <li class="bg<?=$activeId?> <?=(($key == 4)?'last':'')?> <?=(($key == 0)?'new-menu':'')?>"
                        style="background-position: -<?=$arrayPosition[$key]?>px 0">
                    <a id="bg<?=$key+1?>" href="<?=$one['url']?>"><?=$one['title']?></a>
                    <?if(!empty($one['submenu'])):?>
                        <?$subMenu=explode(',',$one['submenu'])?>
                    <ul class="sub<?=$key+1?>" style="background-position:-239px 0;">
                        <?foreach($subMenu as $link):?>
                            <?$oneElement=explode('|',$link);?>
                            <li><a href="<?=$oneElement[1]?>"><?=$oneElement[0]?></a></li>
                        <?endforeach;?>
                    </ul>
                    <?endif;?>
                </li>
            <?endforeach;?>
        </ul>
    </div>
</div>
<script type="text/javascript" src="/js/jquery.bgpos.js"></script>

<script type="text/javascript">
    $(function() {
        var current = 0;
        var loaded  = 0;
        for(var i = 1; i <6; ++i)
            $('<img />').load(function(){
                ++loaded;
                if(loaded == 5){
                    $('#bg1,#bg2,#bg3,#bg4,#bg5').mouseover(function(e){
                        var $this = $(this);
                        if($this.parent().index() == current)
                            return;
                        var item = e.target.id;
                        if(item == 'bg1' || current == 4)
                            $('#menu .sub'+parseInt(current+1)).stop().animate({backgroundPosition:"(-239px 0)"},300,function(){
                                $(this).find('li').hide();
                            });
                        else
                            $('#menu .sub'+parseInt(current+1)).stop().animate({backgroundPosition:"(189px 0)"},300,function(){
                                $(this).find('li').hide();
                            });
                        if(item == 'bg1' || current == 4){

                            $('#menu > li').animate({backgroundPosition:"(-999px 0)"},0).removeClass('bg1 bg2 bg3 bg4 bg5').addClass(item);
                            move(1,item);
                        }
                        else{
                            $('#menu > li').animate({backgroundPosition:"(999px 0)"},0).removeClass('bg1 bg2 bg3 bg4 bg5').addClass(item);
                            move(0,item);
                        }
                        if(current == 4 && item == 'bg1'){
                            $('#menu .sub'+parseInt(current)).stop().animate({backgroundPosition:"(-239px 0)"},300);
                        }
                        if(current == 0 && item == 'bg5'){
                            $('#menu .sub'+parseInt(current+2)).stop().animate({backgroundPosition:"(189px 0)"},300);
                        }
                        current = $this.parent().index();
                        $('#menu .sub'+parseInt(current+1)).stop().animate({backgroundPosition:"(0 0)"},300,function(){
                            $(this).find('li').fadeIn();
                        });
                    });
                }
            }).attr('src', '/../images/slider/'+i+'.jpg');
        function move(dir,item){
            if(dir){
                $('#bg1').parent().stop().animate({backgroundPosition:"(0 0)"},200);
                $('#bg2').parent().stop().animate({backgroundPosition:"(-239px 0)"},300);
                $('#bg3').parent().stop().animate({backgroundPosition:"(-428px 0)"},400);
                $('#bg4').parent().stop().animate({backgroundPosition:"(-617px 0)"},500);
                $('#bg5').parent().stop().animate({backgroundPosition:"(-806px 0)"},600,function(){
                    $('#menuWrapper').removeClass('bg1 bg2 bg3 bg4 bg5').addClass(item);
                });
            }
            else{
                $('#bg1').parent().stop().animate({backgroundPosition:"(0 0)"},400,function(){
                    $('#menuWrapper').removeClass('bg1 bg2 bg3 bg4 bg5').addClass(item);
                });
                $('#bg2').parent().stop().animate({backgroundPosition:"(-239px 0)"},300);
                $('#bg3').parent().stop().animate({backgroundPosition:"(-428px 0)"},300);
                $('#bg4').parent().stop().animate({backgroundPosition:"(-617px 0)"},300);
                $('#bg5').parent().stop().animate({backgroundPosition:"(-806px 0)"},200);
            }
        }
    });
</script>