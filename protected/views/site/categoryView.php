<div class="category-popup">
    <div class="about-category">
        <h1><?=$model->title?></h1>
        <p><?=$model->content?></p>
    </div>
    <div class="list-category">
        <?php if (!empty($category)):?>
        <?php foreach($category as $item):?>
            <?= CHtml::link('<p class="title">'.$item->title.'</p><p class="about">'.$item->content.'</p>',array('/posts/index', 'id'=>$item->id),array('class'=>'item-category'))?>
        <?php endforeach?>
        <?php else :?>
            <a class="item-category">
                Пока нет категорий :'(
            </a>
        <?php endif ?>
    </div>
</div>