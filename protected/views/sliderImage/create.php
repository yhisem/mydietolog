<?php
/** @var $this ImageController
 * @var $form CActiveForm
 * @var $model Image
 */
?>
<?php $form = $this->beginWidget('CActiveForm', array(
    'id'=>'images-form',
    'enableAjaxValidation'=>false,
    'htmlOptions' => array('enctype' => 'multipart/form-data'), // ADD THIS
)); ?>
<?=$form->textField($model,'id',array('style'=>'display:none;')); ?>
<div class="field">
    <?=$form->dropDownList($model,'ids',array(1=>1,2=>2,3=>3,4=>4,5=>5))?>
</div>
<div class="field">
    <?php $this->widget('CMultiFileUpload', array(
        'model'      => $model,
        'attribute'  => 'extension',
        'accept'     => 'jpg|gif|png|jpeg',
        'max' => 1,
        'duplicate'  => Yii::t('main','Файл загружен'),
        'options'    => array(),
    ));?>
</div>
<div class="field1">
    <?=CHtml::submitButton('Save'); ?>
</div>
<?php $this->endWidget(); ?>

