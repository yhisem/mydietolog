<?php
/**
 * @var $this SliderImageController
 * @var $form CActiveForm
 * @var $model Slider
 */
?>
<div class="main2">
    <div class="allert">
        <div class="center" style="width:100% ">
            <div class="name">
                <?php $form=$this->beginWidget('CActiveForm', array(
                    'id'=>'developer-form',
                    'enableAjaxValidation'=>false,
                )); ?>
                <?php echo $form->errorSummary($model); ?>
                <p><span><?php echo $form->labelEx($model,'title'); ?></span>
                    <?php echo $form->textField($model,'title',array('class'=>'inp12','maxlength' =>128)); ?></p>
                <p><span><?php echo $form->labelEx($model,'url'); ?></span>
                    <?php echo $form->textField($model,'url',array('class'=>'inp12','maxlength' =>256)); ?></p>
                <p><?php echo CHtml::submitButton('Сохранить',array('class'=>'form-post-views button-main input-but ')); ?></p>
                <?php $this->endWidget(); ?>
            </div>
        </div>
    </div>
</div>




