<?php
/**
 * @var $this PostsController
 * @var $form CActiveForm
 * @var $model Posts
 */
?>
<div class="main2">
    <div class="header2">Вы хотите создать запись</div>
    <div class="allert">

        <div class="center" style="width:100% ">
            <div class="header31">Создание новой записи</div>
            <div class="name">
                <?php $form=$this->beginWidget('CActiveForm', array(
                    'id'=>'developer-form',
                    'enableAjaxValidation'=>false,
                )); ?>
                <?php echo $form->errorSummary($model); ?>
                <p><span><?php echo $form->labelEx($model,'title'); ?></span>
                    <?php echo $form->textField($model,'title',array('class'=>'inp12','maxlength' =>128)); ?></p>
                <p><span><?php echo $form->labelEx($model,'seoKeywords'); ?></span>
                    <?php echo $form->textField($model,'seoKeywords',array('class'=>'inp12','maxlength' =>256)); ?></p>
                <p><span><?php echo $form->labelEx($model,'seoDescription'); ?></span>
                    <?php echo $form->textField($model,'seoDescription',array('class'=>'inp12','maxlength' =>512)); ?></p>
                <p><span><?php echo $form->labelEx($model,'seoTitle'); ?></span>
                    <?php echo $form->textField($model,'seoTitle',array('class'=>'inp12','maxlength' =>512)); ?></p>
                <p><span><?php echo $form->labelEx($model,'categoryId'); ?></span>
                    <?=$form->dropDownList($model,'categoryId',CHtml::listData(Category::model()->findAllByAttributes(array('language'=>'ru')),'id','title'),
                        array('class'=>'inp12','maxlength' =>512)); ?></p>
                <div class="likeP"><span style="display:block;float: none "><?php echo $form->labelEx($model,'content'); ?></span>
                    <div class="editor" style>
                        <div class="panel">
                            <div class="top-panel">
                                <div style="float:right;font: normal 14px Arial">
                                    <a class="open-html-create">
                                        html-теги
                                    </a>
                                </div>
                                <select class="with-title" name="h" onchange="lifeGit.insertTagFromDropBox(this);">
                                    <option value="" class="title">Заголовки:</option>
                                    <option value="h4">Заголовок</option>
                                    <option value="h5">Подзаголовок</option>
                                    <option value="h6">Подподзаголовок</option>
                                </select>
                                <select class="with-title" name="list" onchange="lifeGit.insertList(this);">
                                    <option value="" class="title">Списки:</option>
                                    <option value="ul">UL LI</option>
                                    <option value="ol">OL LI</option>
                                </select>
                                <a title="Жирный" onclick="return lifeGit.insertTagWithText(this, 'b');">
                                    <img width="20" height="20" alt="Ж" src="/images/posts/bold_ru.gif">
                                </a>
                                <a title="Курсив" onclick="return lifeGit.insertTagWithText(this, 'i');">
                                    <img width="20" height="20" alt="К" src="/images/posts/italic_ru.gif">
                                </a>
                                <a title="Подчёркнутый" onclick="return lifeGit.insertTagWithText(this, 'u');">
                                    <img width="20" height="20" alt="__" src="/images/posts/underline_ru.gif">
                                </a>
                                <a title="Зачёркнутый" onclick="return lifeGit.insertTagWithText(this, 's');">
                                    <img width="20" height="20" alt="—" src="/images/posts/strikethrough.gif">
                                </a>
                                <a title="Абзац" onclick="return lifeGit.insertTagWithText(this, 'p');"">
                                <img width="20" height="20" alt="Цитата" src="/images/posts/blockquote.png">
                                </a>
                                <a title="Вставить ссылку" onclick="return lifeGit.insertLink(this);">
                                    <img width="20" height="20" alt="A" src="/images/posts/link.gif">
                                </a>
                                <a title="Вставить изображение" onclick="return lifeGit.insertImage(this);">
                                    <img width="20" height="20" alt="IMG" src="/images/posts/image.gif">
                                </a>
                                <a style="margin-left: 10px;" title="NewsCut" onclick="return lifeGit.insertNewsCut(this);">
                                    <img width="20" height="20" alt="NewsCut" src="/images/posts/cut.gif">
                                </a>
                                <div class="help-explain hidden">
                                    <div class="block_semi">
                                        <span>&lt;NewsCut&gt;</span>
                                        <span class="description">Используется только в текстах постов, скрывает под кат часть текста, следующую за тегом (будет написано «Читать дальше»).</span>
                                    </div>
                                    <div class="close_html">
                                        <a class="open-html-create">закрыть</a></div>
                                </div>
                            </div>
                        </div>
                        <div class="text-holder">
                            <?php echo $form->textArea($model,'content', array(
                                'id'=>'text_textarea',
                                'class'=>'inp21',
                                'style'=>'width:100%;height:350px;font:normal 14px Arial;')); ?>
                        </div>
                    </div>
                </div>

                <p><?php echo CHtml::submitButton('Сохранить',array('class'=>'form-post-views button-main input-but ')); ?></p>

                <?php $this->endWidget(); ?>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function(){
        $('.open-html-create').click(function(){
            var $html = $('.help-explain');
            if($html.hasClass('hidden'))
                $html.removeClass('hidden');
            else
                $html.addClass('hidden');
        });
        lifeGit = {
            insertTagWithText: function (link, tagName){
                var startTag = '<' + tagName + '>';
                var endTag = '</' + tagName + '>';
                lifeGit.insertTag(link, startTag, endTag);
                return false;
            },
            insertImage: function(link){
                var src = prompt('Введите src картинки', 'http://');
                if(src){
                    lifeGit.insertTag(link, '<img src="' + src + '" alt="image"/>', '');
                }
                return false;
            },
            insertLink: function(link){
                var href = prompt('Введите URL ссылки', 'http://');
                if(href){
                    lifeGit.insertTag(link, '<a href="' + href + '">', '</a>');
                }
                return false;
            },
            insertUser: function(link){
                var login = prompt('Введите никнейм пользователя', '');
                if(login){
                    lifeGit.insertTag(link, '<hh user="' + login + '"/>', '');
                }
                return false;
            },
            insertNewsCut: function(link){
                lifeGit.insertTag(link, '<NewsCut>', '');
                return false;
            },
            insertTag: function(link, startTag, endTag, repObj){
                var textareaParent = $(link).parents('.editor');

                var textarea = $('textarea',textareaParent).get(0);
                textarea.focus();

                var scrtop = textarea.scrollTop;
                var cursorPos = lifeGit.getCursor(textarea);
                var txt_pre = textarea.value.substring(0, cursorPos.start);
                var txt_sel = textarea.value.substring(cursorPos.start, cursorPos.end);
                var txt_aft = textarea.value.substring(cursorPos.end);
                if(repObj){
                    txt_sel = txt_sel.replace(/\r/g, '');
                    txt_sel = txt_sel != '' ? txt_sel : ' ';
                    txt_sel = txt_sel.replace(new RegExp(repObj.findStr, 'gm'), repObj.repStr);
                }
                if (cursorPos.start == cursorPos.end){
                    var nuCursorPos = cursorPos.start + startTag.length;
                }else{
                    var nuCursorPos=String(txt_pre + startTag + txt_sel + endTag).length;
                }
                textarea.value = txt_pre + startTag + txt_sel + endTag + txt_aft;
                lifeGit.setCursor(textarea, nuCursorPos, nuCursorPos);
                if (scrtop) textarea.scrollTop = scrtop;
                return false;
            },
            insertTagFromDropBox: function(link){
                lifeGit.insertTagWithText(link, link.value);
                link.selectedIndex = 0;
            },
            insertList: function(link){
                var startTag = '<' + link.value + '>\n';
                var endTag = '\n</' + link.value + '>';
                var repObj = {
                    findStr: '^(.+)',
                    repStr: '\t<li>$1</li>'
                };
                lifeGit.insertTag(link, startTag, endTag, repObj);
                link.selectedIndex = 0;
            },
            insertSource: function(select){
                var startTag = '<source lang="' + select.value + '">\n';
                var endTag = '\n</source>';
                lifeGit.insertTag(select, startTag, endTag);
                select.selectedIndex = 0;
            },
            insertTab: function(e, textarea){
                if(!e) e = window.event;
                if (e.keyCode) var keyCode = e.keyCode;
                else if (e.which) var keyCode = e.which;
                switch(e.type){
                    case 'keydown':
                        if(keyCode == 16){
                            lifeGit.shift = true;
                        }
                        break;
                    case 'keyup':
                        if(keyCode == 16) {
                            lifeGit.shift = false;
                        }
                        break;
                }
                textarea.focus();
                var cursorPos = lifeGit.getCursor(textarea);
                if (cursorPos.start == cursorPos.end){
                    return true;
                } else if(keyCode == 9 && !lifeGit.shift){
                    var repObj = {
                        findStr: '^(.+)',
                        repStr: '\t$1'
                    };
                    LifeGit.insertTag(textarea, '', '', repObj);
                    return false;
                } else if(keyCode == 9 && lifeGit.shift){
                    var repObj = {
                        findStr: '^\t(.+)',
                        repStr: '$1'
                    };
                    lifeGit.insertTag(textarea, '', '', repObj);
                    return false;
                }
            },
            getCursor: function(input){
                var result = {start: 0, end: 0};
                if (input.setSelectionRange){
                    result.start= input.selectionStart;
                    result.end = input.selectionEnd;
                } else if (!document.selection) {
                    return false;
                } else if (document.selection && document.selection.createRange) {
                    var range = document.selection.createRange();
                    var stored_range = range.duplicate();
                    stored_range.moveToElementText(input);
                    stored_range.setEndPoint('EndToEnd', range);
                    result.start = stored_range.text.length - range.text.length;
                    result.end = result.start + range.text.length;
                }
                return result;
            },
            setCursor: function(textarea, start, end){
                if(textarea.createTextRange) {
                    var range = textarea.createTextRange();
                    range.move("character", start);
                    range.select();
                } else if(textarea.selectionStart) {
                    textarea.setSelectionRange(start, end);
                }
            }
        }
    });
</script>



