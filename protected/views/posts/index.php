<?/**
 * @var $dataProvider CSqlDataProvider
 * @var $this PostsController
 * @var $category array()
 * @var $one Category
*/?>
<section id="content">
    <div class="content-bg">
        <div class="container_24">
            <div class="wrapper">
                <article class="grid_8">
                    <div class="indent bcolor-1 prev-indent-bot">
                        <h4 class="p0" style="font-size: 28px"><?=t('posts','Список категорий')?></h4>
                        <ul class="list-2">
                            <?foreach($category as $one):?>
                                <li><?=CHtml::link($one->title,array('posts/index','categoryId'=>$one->id))?></li>
                            <?endforeach;?>
                        </ul>
                    </div>
                    <figure><a href="#"><img src="/images/bunner-1.jpg" alt="" /></a></figure>
                </article>
                <article class="grid_16">
                    <h3 class="margin-top">Спиок статей</h3>
                    <div class="slider-wrapper">
                        <ul id="slider" style="height: auto">
                            <?php if(!empty($dataProvider)): ?>
                                <?php
                                $this->widget('zii.widgets.CListView', array(
                                    'dataProvider'=>$dataProvider,
                                    'itemView'=>'_posts',
                                    'htmlOptions'=>array(
                                        'class' => 'index-pagination'
                                    ),
                                    'ajaxUpdate'=>false,
                                    'template' => '{items}{pager}',
                                    'pager' => array(
                                        'firstPageLabel'=>'',
                                        'prevPageLabel'=>'<span class="a">'.t('posts','Предыдущие').'</span>',
                                        'nextPageLabel'=>'<span class="a2">'.t('posts','Далее').'</span>',
                                        'nextPageCssClass' => 'gogo',
                                        'previousPageCssClass' => 'empty',
                                        'lastPageLabel'=>'',
                                        'maxButtonCount'=>'4',
                                        'header'=>'',
                                        'htmlOptions' => array(
                                            'class'=>'pagination small'
                                        ),
                                    ),
                                ));
                                ?>
                            <?php endif ?>
                        </ul>
                    </div>
                </article>
            </div>
        </div>
    </div>
</section>