<?/**
 * @var $data array()
 * @var $index
*/?>
<li style="height: auto;margin:0 0 20px 0;overflow: hidden">
    <figure class="img-indent2"><img src="/images/page2-img<?=($index+1)?>.jpg" alt="" /></figure>
    <div class="extra-wrap">
        <h6 class="p2"><?=CHtml::link($data['title'],array('/posts/view','id'=>$data['id']))?></h6>
        <?=Posts::smallContent($data['content'])?>
        <?=CHtml::link('Read More',array('/posts/view','id'=>$data['id']),array('class'=>'link-2'))?>
    </div>
</li>