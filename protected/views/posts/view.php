<?/**
 * @var $this PostsController
 * @var $model Posts
*/?>
<div id="content">
    <div class="content-bg">
        <div class="container_24">
            <div class="wrapper">
    <article class="grid_16">
        <h3 class="prev-indent-bot"><?=$model->title?></h3>
        <div class="indent-left"><?=$model->content?></div>
        <div class="wrapper">
<!--            <div class="grid_8 alpha">-->
<!--                <h3>Latest News</h3>-->
<!--                <div class="indent-left">-->
<!--                    <div class="wrapper indent-bot">-->
<!--                        <time class="tdate-1" datetime="2012-09-25">25<strong>sep</strong></time>-->
<!--                        <div class="extra-wrap color-2">-->
<!--                            <strong class="title-4">Lorem ipsum</strong>-->
<!--                            Praesent vestibulum molestie lacus. aenean nonummy hendrerit maurisu phasellus porta <a class="color-4" href="#">...</a>-->
<!--                        </div>-->
<!--                    </div>-->
<!--                    <div class="wrapper indent-bot">-->
<!--                        <time class="tdate-1" datetime="2012-09-19">19<strong>sep</strong></time>-->
<!--                        <div class="extra-wrap color-2">-->
<!--                            <strong class="title-4">in dipiscing</strong>-->
<!--                            Praesent vestibulum molestie lacus. aenean nonummy hendrerit maurisu phasellus porta <a class="color-4" href="#">...</a>-->
<!--                        </div>-->
<!--                    </div>-->
<!--                    <a class="link-2" href="#">View All</a>-->
<!--                </div>-->
<!--            </div>-->

<!--            <div class="grid_8 omega">-->
<!--                <h3>Our Clients Say... </h3>-->
<!--                <blockquote class="indent-bot">-->
<!--                    <div class="padding">-->
<!--                        <p>Morbi nunc odio, gravida at, cursus nec, luctus a, lorem. Maecenas tristique orci acsem duis ultricies pharetra magna donec accumsan...</p>-->
<!--                        <h6 class="color-3">John Anderson</h6>-->
<!--                        client-->
<!--                    </div>-->
<!--                </blockquote>-->
<!--                <div class="indent-left">-->
<!--                    <a class="link-2" href="#">All Testimonials</a>-->
<!--                </div>-->
<!--            </div>-->

        </div>
    </article>
    <article class="grid_8">
        <div class="indent-top2">
            <div class="indent bcolor-1">
                <div class="carousel">
                    <h4 class="indent-bot">Our Team:</h4>
                    <a class="_next">next</a>
                    <a class="_prev">prev</a>
                    <div class="jCarouselLite">
                        <ul class="carousel-list">
                            <li>
                                <div class="img-indent2"><img src="/images/page4-img2.jpg" alt="" /></div>
                                <h6 class="prev-indent-bot"><a href="#">John Franklin</a></h6>
                                Basnim ipsam lertase volupta jertyades
                            </li>
                            <li>
                                <div class="img-indent2"><img src="/images/page4-img3.jpg" alt="" /></div>
                                <h6 class="prev-indent-bot"><a href="#">Inga North</a></h6>
                                Basnim ipsam lertase volupta jertyades
                            </li>
                            <li>
                                <div class="img-indent2"><img src="/images/page4-img4.jpg" alt="" /></div>
                                <h6 class="prev-indent-bot"><a href="#">Patrick Pool</a></h6>
                                Basnim ipsam lertase volupta jertyades
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </article>
</div>
        </div>
    </div>
</div>