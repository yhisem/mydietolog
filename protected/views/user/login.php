<div>
    <?php $form = $this->beginWidget('CActiveForm', array('id' => 'user-login', 'enableAjaxValidation' => false,)); ?>
    <div class = "title-enter-main">
        <div class = "label-block">
            <?php echo $form->label($loginModel, 'email', array('class' => 'registration-label', 'label' => 'Ваш Email'));?>
            <?php echo $form->label($loginModel, 'password', array('class' => 'registration-label', 'label' => 'Пароль'));?>
        </div>
        <div class = "input-block">
            <div>
                <?php echo $form->textField($loginModel, 'email', array('size' => 40, 'maxlength' => 128)); ?>
                <?php $form->error($loginModel, 'email'); ?>
            </div>
            <div>
                <?php echo $form->passwordField($loginModel, 'password', array('size' => 40, 'maxlength' => 128)); ?>
                <?php $form->error($loginModel, 'password'); ?>
            </div>
        </div>
    </div>
    <div class = "submit-box">
        <?php echo CHtml::submitButton('Войти', array('class' => 'enter-blick', 'name' => 'UserLogin'));?>
        <!--<p class = "remember">Забыл пароль?</p>-->
    </div>
    <?php $this->endWidget()?>
</div>