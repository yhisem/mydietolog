<?php
/**
 * @var $this CategoryController
 * @var $form CActiveForm
 * @var $model Category
 */
?>
<section id="content">
    <div class="content-bg">
        <div class="container_24">
            <div class="main2">
                <div class="allert">
                    <div class="center" style="width:100% ">
                        <div class="name">
                            <?php $form=$this->beginWidget('CActiveForm', array(
                                'id'=>'developer-form',
                                'enableAjaxValidation'=>false,
                            )); ?>
                            <?=$form->errorSummary($model); ?>
                            <p><span><?=$form->labelEx($model,'title'); ?></span>
                                <?=$form->textField($model,'title',array('class'=>'inp12','maxlength' =>128)); ?></p>
                            <p><span><?=$form->labelEx($model,'seoTitle'); ?></span>
                                <?=$form->textField($model,'seoTitle',array('class'=>'inp12','maxlength' =>128)); ?></p>
                            <p><span><?=$form->labelEx($model,'seoKeywords'); ?></span>
                                <?=$form->textField($model,'seoKeywords',array('class'=>'inp12','maxlength' =>256)); ?></p>
                            <p><span><?=$form->labelEx($model,'seoDescription'); ?></span>
                                <?=$form->textField($model,'seoDescription',array('class'=>'inp12','maxlength' =>512)); ?></p>
                            <p><span><?=$form->labelEx($model,'seoTitle'); ?></span>
                                <?=$form->textArea($model,'content',array('class'=>'inp12','maxlength' =>1024)); ?></p>
                            <p><span><?=$form->labelEx($model,'parentId'); ?></span>
                                <?=$form->dropDownList($model,'parentId',
                                    CMap::mergeArray(array('0'=>'main'),CHtml::listData(Category::model()->findAllByAttributes(array('parentId'=>0)),'id','title')),
                                    array('class'=>'inp12','maxlength' =>1024)); ?></p>

                            <p><?php echo CHtml::submitButton('Сохранить',array('class'=>'form-post-views button-main input-but ')); ?></p>

                            <?php $this->endWidget(); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>




