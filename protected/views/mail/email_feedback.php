<?php
/**
 * @var CController $this
 * @var Feedback $model
 */?>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv = "Content-Type" content = "text/html; charset=utf-8"/>
    <title></title>
</head>
<body style="margin: 0;background-color: #ffffff;">
<div style ="width: 572px;margin: 10px auto 60px;">
    <div style = "font-size: 12px;font-family: Arial;color: #c8c8c8; text-decoration: underline;text-align: center;cursor: pointer;margin-bottom: 21px;"><?=t('mail','ФИО')?> : <?=$model->name?></div>
    <div style="background: rgb(211, 227, 230);background: -moz-linear-gradient(90deg, rgb(211, 227, 230) 0%, rgb(239, 245, 246) 100%);background: -webkit-linear-gradient(90deg, rgb(211, 227, 230) 0%, rgb(239, 245, 246) 100%);background: -o-linear-gradient(90deg, rgb(211, 227, 230) 0%, rgb(239, 245, 246) 100%);background: -ms-linear-gradient(90deg, rgb(211, 227, 230) 0%, rgb(239, 245, 246) 100%);background: linear-gradient(180deg, rgb(211, 227, 230) 0%, rgb(239, 245, 246) 100%);width: 100%;height: 6px"></div>
    <div style="width: 480px;border:1px solid #d3e3e6; margin: 0 auto 40px ;padding: 40px 50px 90px 40px">
        <p style="font-weight: bold;font-size: 18px;font-family: Arial;color: #000"><?=t('mail','Email')?> : <?=$model->email?></p>
        <p style="margin: 25px 0 25px 0;font-family: Arial;font-size: 14px;"><?=t('mail','Phone')?> : <?=$model->phone?></p>
        <p style="margin: 25px 0 25px 0;font-family: Arial;font-size: 14px;"><?=$model->content?></p>
    </div>
    <div style="overflow: hidden;position: relative;width: 495px;padding: 0 45px 0 40px; ">
        <div style="width: 170px;float: left;">
            <p style="font-size: 12px;font-family: arial;color:#1d1d1d;line-height:5px "><?=t('mail','Наши контакти')?>:</p>
            <p style="font-size: 12px;font-family: arial;color:#1d1d1d;line-height:5px"><?=t('mail','E-mail')?>:<span style="font-weight: bold;!important;font-size: 12px;font-family: arial;color:#1d1d1d;!important;line-height:5px;"> service@mydietolog.kiev.ua</span> </p>
            <p style="font-size: 12px;font-family: arial;color:#1d1d1d;line-height:5px"><?=t('mail','Сайт')?>: <a href = "http://mydietolog.keiv.ua" style="font-weight: bold;font-size: 12px;font-family: arial;color:#1d1d1d;line-height:5px;text-decoration: none;"> www.mydietolog.kiev.ua</span></a>
        </div>
        <div style="width: 130px;float: left;padding: 0 0 0 50px;">
            <p style="font-size: 12px;font-family: arial;color:#1d1d1d;line-height:5px;font-weight: bold; ">(044) 444-44-44</p>
            <p style="font-size: 12px;font-family: arial;color:#1d1d1d;line-height:5px;"><?=t('mail','будем рады помочь')?></p>
        </div>
        <div style="width: 100px;float: right;padding: 0 0 0 45px;position: relative; overflow: hidden;">
            <p style="font-size: 12px;font-family: arial;color:#1d1d1d;line-height:5px;">© 2012 MyDietolog</p>
        </div>
    </div>
</div>
</body>
</html>