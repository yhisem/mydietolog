<?php
/**
 * @var Controller $this
 * @var $content
 */
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<?$this->renderPartial('//block/head')?>
<body id="page1">
    <div class="border-top">
        <div class="main">
            <div class="wrapper">
                <?php include(dirname(__FILE__)."/../block/header.php");?>
                <main id="content">
                    <?php echo $content?>
                </main>
            </div>
            <?php include(dirname(__FILE__)."/../block/footer.php");?>
        </div>
    </div>
</body>
</html>

