<section id="content">
    <div class="content-bg">
        <div class="container_24">
            <div class="wrapper">
                <article class="grid_16">
                    <h3><?=t('info','Наши контакти')?></h3>
                    <div class="indent-left">
                        <p class="indent-bot">Fusce euismod consequat ante. Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Pellentesque sed dolor. Aliquam congue fermentum nisl. Mauris accumsan nulla vel diam. Sed in lacus ut enim adipiscing aliquet. Nulla venenatis. In pede mi, aliquet sit amet, euismod.</p>
                        <div class="wrapper">
                            <figure class="img-indent2" style="width:430px;">
                                <?
                                Yii::import('ext.EGMap.*');
                                $gMap = new EGMap();
                                $gMap->setWidth(430);
                                $gMap->setHeight(400);
                                $gMap->zoom = 7;
                                $gMap->setCenter(50.0 , 29.45);
                                $icon = new EGMapMarkerImage("http://unikasa.net/images/body/point.png");
                                $icon->setSize(25, 32);
                                $icon->setAnchor(16, 16.5);
                                $icon->setOrigin(0, 0);

                                $marker = new EGMapMarkerWithLabel(49.238673, 28.469696, array('icon' => $icon, 'title' => 'name'));
                                $marker->addHtmlInfoWindow(new EGMapInfoWindow(CHtml::tag('p', array(), 'name')));
                                $gMap->addMarker($marker);

                                $marker = new EGMapMarkerWithLabel(50.452067, 30.46483, array('icon' => $icon, 'title' => 'name1'));
                                $marker->addHtmlInfoWindow(new EGMapInfoWindow(CHtml::tag('p', array(), 'name1')));
                                $gMap->addMarker($marker);

                                $gMap->renderMap();
                                ?>
                            </figure>
                            <div class="extra-wrap">
                                <dl>
                                    <dt>8901 Marmora Road,<br>Glasgow, D04 89GR.</dt>
                                    <dd><span>Freephone:</span>  +1 800 559 6580</dd>
                                    <dd><span>Telephone:</span>  +1 800 603 6035</dd>
                                    <dd><span>FAX:</span>  +1 800 889 9898</dd>
                                    <dd>E-mail: <a class="link" href="#"><strong>mail@demolink.org</strong></a></dd>
                                </dl>
                            </div>
                        </div>
                    </div>
                </article>
                <article class="grid_8">
                    <div class="indent-top3">
                        <?$this->renderPartial('//block/feedback')?>
                    </div>
                </article>
            </div>
        </div>
    </div>
</section>