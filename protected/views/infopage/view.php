<?/**
 * @var $model Infopage
 * @var $infoArray array()
 * @var $info Infopage
 */?>
<section id="content">
    <div class="content-bg">
        <div class="container_24">
            <div class="wrapper">
                <article class="grid_16">
                    <h3><?=$model->title?></h3>
                    <div class="indent-left">
                        <?=$model->content?>
                    </div>
                </article>
                <article class="grid_8">
                    <div class="indent-top3">
                        <div class="indent bcolor-1">
                            <h4 class="p2" style="font-size: 30px">Інші відомості</h4>
                            <ul class="list-2">
                                <?foreach($infoArray as $info):?>
                                    <li><?=CHtml::link($info->title,array('/infopage/view','slug'=>$info->slug))?></li>
                                <?endforeach;?>
                            </ul>
                        </div>
                    </div>
                </article>
            </div>
        </div>
    </div>
</section>