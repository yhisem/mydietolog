<div class="indent bcolor-1">
    <h4 class="p2 title-feedback"  style="font-size: 27px"><?=t('block','Обратная связь')?></h4>
    <form id="contact-form">
        <div class="success"> Contact form submitted!<br>
            <strong>We will be in touch soon.</strong> </div>
        <fieldset>
            <label class="name">
                <input id="feedback-fio" type="text" value="" placeholder="<?=t('block','ФИО')?>">
                <span class="error">*This is not a valid name.</span> <span class="empty">*This field is required.</span>
            </label>
            <label class="email">
                <input id="feedback-mail" type="text" value="" placeholder="E-mail">
                <span class="error">*This is not a valid email address.</span> <span class="empty">*This field is required.</span>
            </label>
            <label class="phone">
                <input id="feedback-phone" type="text" value="" placeholder="<?=t('block','Телефон')?>">
                <span class="error">*This is not a valid phone number.</span> <span class="empty">*This field is required.</span>
            </label>
            <label class="message">
                <textarea id="feedback-text" placeholder="<?=t('block','Текст сообшения')?>"></textarea>
                <span class="error">*Is too short message.</span> <span class="empty">*This field is required.</span> </label>

            <div class="feedbackErrorMessage" style="font: normal 13px Arial;color:#ec8e83;"></div>
            <div class="buttons-wrapper">
                <a id="clear-feedback" class="link-2" data-type="reset"><?=t('block','Очистить')?></a>
                <a id="feedback-send" class="link-2" data-type="submit"><?=t('block','Послать')?></a>
            </div>
        </fieldset>
    </form>
    <script>
        $(document).ready(function(){
            var $fio = $('#feedback-fio');
            var $mail = $('#feedback-mail');
            var $phone = $('#feedback-phone');
            var $text = $('#feedback-text');
            function clearFeedback(){
                $fio.val('');
                $mail.val('');
                $phone.val('');
                $text.val('');
            }
            $('#clear-feedback').click(function(){
               clearFeedback();
            });
            $('#feedback-send').click(function(){
                var $error = $('.feedbackErrorMessage');
                $.ajax({
                    type: "POST",
                    data: {'name':$fio.val(),'email':$mail.val(),'phone':$phone.val(),'text':$text.val()},
                    url: '/feedback/send',
                    dataType: "json"
                }).done(function(data){
                    clearFeedback();
                    if(data.answer)
                        $error.html('<?=t('block','Ваше сообщение будет рассмотрено в ближайшем времени')?>');
                    else $error.html('<?=t('block','Ошибка')?>');
                });
            });
        });
    </script>
</div>