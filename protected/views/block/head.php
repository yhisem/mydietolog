<head>
    <title><?=$this->pageTitle;?></title>
    <meta charset="utf-8">
    <link rel="stylesheet/less" type="text/css" href="/less/styles.less">
    <link rel="stylesheet" href="/css/reset.css" type="text/css" media="screen">
    <link rel="stylesheet" href="/css/style.css" type="text/css" media="screen">
    <link rel="stylesheet" href="/css/style.css" type="text/css" media="screen">
    <link rel="stylesheet" href="/css/grid.css" type="text/css" media="screen">
    <script src="/js/less.js" type="text/javascript"></script>
    <script src="/js/popup.js" type="text/javascript"></script>
    <?php Yii::app()->clientScript->registerPackage('jquery');?>

<!--    <div style=' clear: both; text-align:center; position: relative;'> -->
<!--        <a href="http://windows.microsoft.com/en-US/internet-explorer/products/ie/home?ocid=ie6_countdown_bannercode">-->
<!--            <img src="http://storage.ie6countdown.com/assets/100/images/banners/warning_bar_0000_us.jpg" border="0" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today." />-->
<!--        </a>-->
<!--    </div>-->
<!--    <![endif]-->
    <!--[if lt IE 9]>
    <script type="text/javascript" src="/js/html5.js"></script>
    <link rel="stylesheet" href="/css/ie.css" type="text/css" media="screen">
    <![endif]-->
    <?php Yii::app()->clientScript->registerPackage('jquery');?>
</head>