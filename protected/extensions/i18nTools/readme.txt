1. Configure UrlManager like
		'urlManager' => array(
            'class'=>'ext.titI18nTools.components.UrlManager',
            'showScriptName' => false,
			'urlFormat' => 'path',
			'rules' => array(
                '<language:(uk|ru|en)>/' => 'index/index',
                '<language:(uk|ru|en)>/<controller:\w+>/<action:\w+>/*'=>'<controller>/<action>',
			),
		),

2. Add behaviour to components/Controller.php
    public function behaviors()
    {
        return array(
            'i18n' => array(
                'class' => 'ext.titI18nTools.behaviors.I18nControllerBehavior',
            ),
        );
    }


3. Add languages property to app params
    'params' => array(
        'languages'=>array(
                    'uk'=>array(
                        'title'=>'Українська',
                        'substitute'=>array('uk'),
                    ),
                    'ru'=>array(
                        'title'=>'Русский',
                        'substitute'=>array('ru','be','ky','ab','mo','et','lv'),
                    ),
                ),
	),

3. Install LanguageSelector where you want
    <?php $this->widget('ext.i18nTools.widgets.LanguageSelector',array(
                'type'=>'dropDownList',
            )); ?>

    Params : type;
    Where 'type'=>'dropDownList','flags','links'  - for select type


OR add to CMenu something like
    $langs=array();
    foreach (Yii::app()->params['languages'] as $lang=>$langName)
    {
        $langs[]=array('label' => $langName,
            'url' => $this->createMultilanguageReturnUrl($lang),
            'enabled'=>$lang== Yii::app()->language,
        );

    }

    ...
    'items' => array(
        array(
            'label' => Yii::app()->params['languages'][Yii::app()->language],
            'url' => '#',
            'items' => $langs
        ),
    )
    ...
