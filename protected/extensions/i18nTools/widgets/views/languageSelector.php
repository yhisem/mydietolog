<div id="language-select">
    <?php
        if(($this->type==='default' && count($languages) == 2)|| $this->type==='flags')
        {
            foreach($languages as $key=>$lang)
            {
                if($key != $currentLang)
                {
                    echo CHtml::link('<div class="'.$key.'-active"></div>',$this->getOwner()->createMultilanguageReturnUrl($key),
                        array('class'=>'for-language'));
                }
                else
                {
                    echo '<div class="for-language for-language-active '.$key.'"></div>';
                }
            }
        }
        elseif(($this->type==='default' && count($languages) < 4) || $this->type==='links') {
            // Render options as links
            $lastElement = end($languages);
            foreach($languages as $key=>$lang) {
                if($key != $currentLang) {
                    echo CHtml::link(
                        $lang['title'],
                        $this->getOwner()->createMultilanguageReturnUrl($key),
                        array('class'=>'for-language'));
                } else echo '<b class="for-language for-language-active">'.$lang['title'].'</b>';
                if($lang != $lastElement) echo ' | ';
            }
        }
        elseif ( ($this->type==='default' && count($languages) >= 4) || $this->type==='dropDownList' || $this->type==='dropDownList-simple') {
            // Render options as dropDownList
            foreach($languages as $key=>$lang) {
                echo CHtml::hiddenField(
                    $key,
                    $this->getOwner()->createMultilanguageReturnUrl($key));
                $array[$key]=$lang['title'];
            }
            echo '<select name="language" id="language">';
            foreach($languages as $key=>$lang)
            {
                if($key != $currentLang)
                {
                    echo "<option class='".$key."-lang' value='".$key."'>".$lang['title']."</option>" ;
                }
                else
                {
                    echo "<option class='".$key."-lang' selected='selected' value='".$key."'>".$lang['title']."</option>" ;
                }

            };
            echo '</select> ';
        }
    ?>
</div>