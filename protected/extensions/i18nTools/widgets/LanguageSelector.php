<?php
class LanguageSelector extends CWidget
{
    public $type='default';
    public $publishedAssets;

    public function run()
    {
        $this->publishedAssets = Yii::app()->getAssetManager()->publish(dirname(__FILE__) . "/assets");
        $currentLang = Yii::app()->language;
        $languages = Yii::app()->params->languages;
        /**
         * @var $cs CClientScript
         */
        $cs = Yii::app()->clientScript;
        $cs->registerCssFile($this->publishedAssets . '/main.css');
        if($this->type==='dropDownList' || $this->type==='dropDownList-simple' || ($this->type==='default' AND count($languages)>=4))
        {
            $cs->registerScriptFile($this->publishedAssets . '/select-language.js');

            if($this->type!== 'dropDownList-simple')
            {
                $packName = "jquery.formStyler";
                $cs->registerCoreScript($packName);

                if ($cs->getPackageBaseUrl($packName)===false)
                {
                    $cs->addPackage($packName, array(
                        'baseUrl'=>$this->publishedAssets."/formStyler/",
                        'js'=>array(YII_DEBUG?'jquery.formstyler.js':'jquery.formstyler.min.js'),
                        'css'=>array('jquery.formstyler.css'),
                        'depends'=>array('jquery'),
                    ));
                    $cs->registerCoreScript($packName);
                }
            }
        }
        $this->render('languageSelector', array('currentLang' => $currentLang, 'languages'=>$languages, 'type'=>$this->type));
    }
}
