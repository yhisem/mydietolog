<?php
/**
 * This is the configuration for generating message translations
 * for the Yii framework. It is used by the 'yiic message' command.
 */
return array(
    'sourcePath'=>dirname(__FILE__).'/..',
    'messagePath'=>dirname(__FILE__),
    'languages'=>array('uk'),
    'fileTypes'=>array('php'),
    'overwrite'=>true,
    'translator'=>"t",
    'sort'=>true,
    'exclude'=>array(
        '.svn',
        '.git',
        '.gitignore',
        'yiilite.php',
        'yiit.php',
        '/i18n/data',
        '/messages',
        '/vendors',
        '/web/js',
        'Sass.php',
        'Saas.php',
        'web/media',
    ),
);
