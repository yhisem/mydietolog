<?php

/**
 * This is the model class for table "Category".
 *
 * The followings are the available columns in table 'Category':
 * @property integer $id
 * @property integer $parentId
 * @property string $title
 * @property string $content
 * @property string $seoTitle
 * @property string $seoKeywords
 * @property string $seoDescription
 * @property string $language
 */
class Category extends CActiveRecord
{

    public function tableName()
    {
        return 'Category';
    }

    public function rules()
    {
        return array(
            array('parentId', 'numerical', 'integerOnly'=>true),
            array('title, seoTitle', 'length', 'max'=>128),
            array('seoKeywords', 'length', 'max'=>256),
            array('content', 'length', 'max'=>2048),
            array('seoDescription', 'length', 'max'=>512),
            array('language', 'length', 'max'=>3),
            array('id, title,language, content, seoTitle,parentId,seoKeywords,seoDescription', 'safe', 'on'=>'search'),

        );
    }

    public function relations()
    {
        return array(
        );
    }

    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'title' => 'Заголовок',
            'content' => 'Текст',
            'seoKeywords' => 'Кейворды',
            'authorId' => 'Автор',
            'seoDescription' => 'Описание',
        );
    }

    public function search()
    {
        $criteria=new CDbCriteria;

        $criteria->compare('id',$this->id);
        $criteria->compare('title',$this->title,true);
        $criteria->compare('seoTitle',$this->seoTitle,true);
        $criteria->compare('content',$this->content,true);
        $criteria->compare('seoDescription',$this->seoDescription,true);
        $criteria->compare('language',$this->language,true);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
        ));
    }

    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    public static function smallContent($content)
    {
        return mb_strcut($content, 0 , 130 ,"UTF-8").'...';
    }
}
