<?php

/**
 * This is the models class for table "Infopage".
 *
 * The followings are the available columns in table 'Infopage':
 * @property integer $id
 * @property string $title
 * @property string $content
 * @property string $slug
 * @property string $seoTitle
 * @property string $seoDescription
 * @property string $seoKeywords
 * @property string $language
 */
class Infopage extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'Infopage';
	}

	public function rules()
	{
		return array(
			array('title, slug, seoTitle, id', 'length', 'max'=>128),
			array('seoDescription', 'length', 'max'=>512),
			array('seoKeywords', 'length', 'max'=>256),
			array('language', 'length', 'max'=>3),
			array('content', 'safe'),
			array('id, title,language, content, slug, seoDescription,seoTitle, seoKeywords', 'safe', 'on'=>'search'),
		);
	}

	public function relations()
	{
		return array(
		);
	}

	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'title' => 'Title',
			'timeCreate' => 'Time Create',
			'content' => 'Content',
			'slug' => 'Slug',
			'seoDescription' => 'Seo Description',
			'seoKeywords' => 'Seo Keywords',
		);
	}

	public function search()
	{

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('seoTitle',$this->seoTitle,true);
		$criteria->compare('content',$this->content,true);
		$criteria->compare('slug',$this->slug,true);
		$criteria->compare('seoDescription',$this->seoDescription,true);
		$criteria->compare('seoKeywords',$this->seoKeywords,true);
		$criteria->compare('language',$this->language,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

}
