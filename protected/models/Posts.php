<?php

/**
 * This is the model class for table "Posts".
 *
 * The followings are the available columns in table 'Posts':
 * @property integer $id
 * @property string $title
 * @property string $content
 * @property string $seoTitle
 * @property string $seoKeywords
 * @property string $seoDescription
 * @property integer $authorId
 * @property integer $categoryId
 * @property integer $language
 */
class Posts extends CActiveRecord
{
	public function tableName()
	{
		return 'Posts';
	}

	public function rules()
	{
		return array(
			array('authorId, categoryId, id', 'numerical', 'integerOnly'=>true),
			array('title, seoTitle', 'length', 'max'=>128),
			array('seoKeywords', 'length', 'max'=>256),
			array('seoDescription', 'length', 'max'=>256),
			array('language', 'length', 'max'=>3),
			array('content', 'safe'),
			array('id, title , content,language, seoTitle,seoKeywords,seoDescription,authorId,categoryId', 'safe', 'on'=>'search'),

		);
	}

	public function relations()
	{
		return array(
		);
	}

	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
		);
	}

	public function search()
	{
		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('seoTitle',$this->seoTitle,true);
		$criteria->compare('content',$this->content,true);
		$criteria->compare('seoKeywords',$this->seoKeywords,true);
		$criteria->compare('authorId',$this->authorId);
		$criteria->compare('seoDescription',$this->seoDescription,true);
		$criteria->compare('language',$this->language,true);
		$criteria->compare('categoryId',$this->categoryId);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}


	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Posts the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public static function smallContent($content,$slug = null){
		$coll = strpos($content,'<NewsCut>');
		$coll1 = strpos($content,'<newscut>');
		if($coll != 0 || $coll1 != 0)
		{
			if($slug == null)
			{
				if($coll > $coll1)
					return substr($content,0,$coll);
				else
					return substr($content,0,$coll1);
			}
			else
			{
				if($coll > $coll1)
					return substr($content,0,$coll);
				else
					return substr($content,0,$coll1);
			}
		}
		else
			return $content;
	}

	public function decodeText($content)
	{
		$patterns = array(
			0=>"/\[b\]/",  1=>"/\[\/b\]/",  2=>"/\[i\]/",  3=>"/\[\/i\]/",  4=>"/\[u\]/",  5=>"/\[\/u\]/",  6=>"/\[p\]/",  7=>"/\[\/p\]/",
			8=>"/\[h4\]/", 9=>"/\[\/h4\]/", 10=>"/\[h5\]/",11=>"/\[\/h5\]/",12=>"/\[h6\]/",13=>"/\[\/h6\]/",14=>"/\[s\]/", 15=>"/\[\/s\]/",
			16=>"/\[ul\]/",17=>"/\[\/ul\]/",18=>"/\[li\]/",19=>"/\[\/il\]/",20=>"/\[ol\]/",21=>"/\[\/ol\]/",22=>"/\[NewsCut\]/",
			23=>"/\[table\]/",24=>"/\[\/table\]/",25=>"/\[tr\]/",26=>"/\[\/tr\]/",27=>"/\[td\]/",28=>"/\[\/td\]/"
		);
		$replacements = array(
			0=>"<b>",  1=>'</b>',  2=>"<i>", 3=>'</i>', 4=>"<u>",   5=>'</u>',  6=>"<p>",   7=>'</p>',  8=>"<h4>",  9=>'</h4>', 10=>"<h5>",11=>'</h5>',
			12=>"<h6>",13=>'</h6>',14=>"<s>",15=>'</s>',16=>'</ul>',17=>'</ul>',18=>'</li>',19=>'</li>',20=>'</ol>',21=>'</ol>',22=>"<NewsCut>",
			23=>"<table>/",24=>"</table>",25=>"<tr>",26=>"</tr>",27=>"<td>",28=>"</td>"
		);

		$content = preg_replace($patterns, $replacements, $content);                                                    // small tags
		$content = preg_replace("~\\[url=([^\\[]+)\\]([^\\[]+)\\[/url\\]~", "<a href='\\1'>\\2</a>", $content);    // link

		$content = preg_replace("~\\[img alt=([^\\[]+)\\ title=([^\\[]+)\\]([^\\[]+)\\[/img\\]~", "<img src='\\3' alt='\\1' title='\\2' alt='images' />", $content); // images
		$content = preg_replace("~\\[img alt=([^\\[]+)\\]([^\\[]+)\\[/img\\]~", "<img src='\\2' alt='\\1' />", $content);                                           // images
		$content = preg_replace("~\\[img title=([^\\[]+)\\]([^\\[]+)\\[/img\\]~", "<img src='\\2' title='\\1' alt='images' />", $content);                           // images
		$content = preg_replace("~\\[img\\]([^\\[]+)\\[/img\\]~", "<img src='\\1' alt='images' title='images' />", $content);                                         // images

		$content = preg_replace("~\\[user=([^\\[]+)\\/\\]~", "<a href=/people/\\1>\\1</a>", $content);             // user
		return $content;
	}

	public function getImageSrc($id)
	{
		/**@var $model Posts*/
		$model = Posts::model()->findByPk($id);
		$content = $model->content;
		if($model->isBBcode == 1)
		{
			$content = preg_replace("~\\[img alt=([^\\[]+)\\ title=([^\\[]+)\\]([^\\[]+)\\[/img\\]~", "[imageHere]\\3[/imageHere]", $content);
			$content = preg_replace("~\\[img alt=([^\\[]+)\\]([^\\[]+)\\[/img\\]~", "[imageHere]\\2[/imageHere]", $content);
			$content = preg_replace("~\\[img title=([^\\[]+)\\]([^\\[]+)\\[/img\\]~", "[imageHere]\\2[/imageHere]", $content);
			$content = preg_replace("~\\[img\\]([^\\[]+)\\[/img\\]~", "[imageHere]\\1[/imageHere]", $content);
		}
		else
			$content = preg_replace('~src=("([^\[]+?)"|\'([^\[]+?)\') ~', '[imageHere]$2$3[/imageHere]', $content);

		$res = substr($content,0,strpos($content , '[/imageHere]'));
		$res = substr($res,strpos($res,'[imageHere]')+11);
		return $res;
	}
}
