<?php

/**
 * This is the model class for table "User".
 *
 * The followings are the available columns in table 'User':
 * @property integer $id
 * @property string $name
 * @property string $password
 * @property string $email
 * @property integer $active
 *
 * The followings are the available model relations:
 * @property Group[] $groups
 * @property Transaction[] $transactions
 * @property Game[] $games
 *
 * Scopes
 * @method User active
 */
class User extends CActiveRecord {
	const INACTIVE = 0;
	const ACTIVE = 1;
	const SCENARIO_REGISTRATION = 'registration';
	const SCENARIO_LOGIN = 'login';

	/**
	 * Returns the static model of the specified AR class.
	 *
	 * @param string $className active record class name.
	 *
	 * @return User the static model class
	 */
	public static function model($className = __CLASS__) {
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName() {
		return 'User';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules() {
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('email', 'email', 'allowEmpty' => false),
			array('email', 'unique', 'on' => array(self::SCENARIO_REGISTRATION)),
			array('email', 'filter', 'filter' => 'mb_strtolower'),
			array('password', 'required', 'on' => array(self::SCENARIO_LOGIN)),
			array('password', 'length', 'max' => 128, 'min' => 6),
			array('password', 'filter', 'filter' => array($this, 'hashPassword'), 'on' => array(self::SCENARIO_REGISTRATION)),
			array('active', 'numerical', 'integerOnly' => true),
			array('name', 'length', 'max' => 10, 'min' => 2),
			array('password', 'authenticate', 'on' => self::SCENARIO_LOGIN),
			array('id, name,  password, email, active', 'safe', 'on' => 'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations() {
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'transactions' => array(self::HAS_MANY, 'Transaction', 'user'),
			'games' => array(self::MANY_MANY, 'Game', 'UserGameReferralBonus(user, game)'),
			'gamesReferralBonus' => array(self::HAS_MANY, 'UserGameReferralBonus', 'user'),
			'gamesDiscount' => array(self::HAS_MANY, 'UserGameDiscount', 'user'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels() {
		return array(
			'id' => 'ID',
			'name' => 'Name',
			'password' => 'Password',
			'email' => 'Email',
			'active' => 'Active',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search() {
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.
		$criteria = new CDbCriteria;
		$criteria->compare('id', $this->id);
		$criteria->compare('name', $this->name, true);
		$criteria->compare('password', $this->password, true);
		$criteria->compare('email', $this->email, true);
		$criteria->compare('active', $this->active);
		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
		));
	}

	public function hashPassword($password) {
		return md5($password);
	}



	public function validatePassword($password) {
//		return md5($password, $this->password) === $this->password;
		return md5($password) === $this->password;
	}

	public function authenticate() {
		$identity = new UserIdentity($this->email, $this->password);
		$identity->authenticate();
		switch ($identity->errorCode) {
			case UserIdentity::ERROR_NONE:
				Yii::app()->user->login($identity);
				break;
			case UserIdentity::ERROR_USERNAME_INVALID:
				$this->addError('email', 'Email address is incorrect.');
				break;
			default:
				$this->addError('password', 'Password is incorrect.');
				break;
		}
	}

	public function getLoginFromEmail() {
		return substr($this->email, 0, strpos($this->email, '@'));
	}

	public function scopes() {
		return array(
			'active' => array(
				'condition' => 'active = :active',
				'params' => array(':active' => self::ACTIVE)
			)
		);
	}



}