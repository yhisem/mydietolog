<?php

// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');

// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.

$A = require('include/urlManager.php');

return CMap::mergeArray($A, array(
    'basePath' => dirname(__FILE__) . DIRECTORY_SEPARATOR . '..',
    'name' => 'MyDietolog.com',
    'defaultController' => 'site',
    'language' => 'ru',
    'sourceLanguage' => 'ru',
    'preload' => array('log'),
    'timeZone' => 'Europe/Kiev',
    'theme'=>'default',
    'import' => array(
        'application.models.*',
        'application.components.*',
        'application.components.utils.*',
        'application.helpers.*',

        'ext.yiiMail.YiiMailMessage',
        'ext.yiiMail.YiiMailMessage2',
    ),

    'modules' => array(
        'gii' => array(
            'class' => 'system.gii.GiiModule',
            'password' => '1111',
            // If removed, Gii defaults to localhost only. Edit carefully to taste.
            'ipFilters' => array('127.0.0.1', '::1'),
        ),
    ),

    // application components
    'components' => array(

        'user' => array(
            // enable cookie-based authentication
            'allowAutoLogin' => true,
            'loginUrl' => array('user/login'),
        ),
        'clientScript'=>array(
            'packages'=>array(
                'jquery'=>array(
                    'baseUrl'=>'/js',
                    'js'=>array(
                        'jquery-1.11.0.min.js',
                        'jquery-migrate-1.2.1.js'
                    ),
                ),
            ),
        ),
        'widgetFactory' => array(
            'widgets' => array(

            ),
        ),
//        'cache' => array(
//            'class' => 'CMemCache',
//            'useMemcached' => false,
//            'keyPrefix' => 'P4D.',
//            'servers' => array(
//                array(
//                    'host' => '127.0.0.1',
//                    'port' => 11211,
//                    'weight' => 1024,
//                ),
//            ),
//        ),
        'authManager' => array(
            'class' => 'CStateRegexAuthManager',
        ),
        'image'=>array(
            'class'=>'ext.image.CImageComponent',
            'driver'=>'GD',
            'params'=>array('directory'=>'/opt/local/bin'),
        ),
        'errorHandler' => array(
            'errorAction' => 'site/error',
        ),
        'session' => array(
            'cookieMode' => 'allow',
            'cookieParams' => array(
                'httponly' => true,
            ),
        ),

        'mail'            => array(
            'class'           => 'ext.yiiMail.YiiMail',
            'transportType'   => 'smtp',
            'transportOptions'=> array(
                'host'    => '77.120.114.139',
                'username'=> 'bot@santa4.me',
                'password'=> 'Jd5D7Rc3',
                'port'    => '25',
            ),
            'viewPath'        => 'application.views.mail',
            'logging'         => true,
            'dryRun'          => false,
            'defaultFrom'     => array('bot@santa4.me' => 'Santa4.me'),
            'defaultBcc'     => array('copy@santa4.me' => 'Santa4.me'),
            'defaultReplyTo'     => array('ask@santa4.me' => 'Santa4.me Support'),
        ),
        'log' => array(
            'class' => 'CLogRouter',
            'routes' => array(
                array(
                    'class' => 'CFileLogRoute',
                    'levels' => 'error, warning',
                ),
            ),
        ),
    ),
    'params' => array(
        'languages'=>array(
            'uk'=>array(
                'title'=>'Українська',
                'substitute'=>array('uk'),
            ),
            'ru'=>array(
                'title'=>'Русский',
                'substitute'=>array('uk','be','ky','ab','mo','et','lv'),
            ),
//            'en'=>array(
//                'title'=>'English',
//                'substitute'=>array('en'),
//            ),
        ),
    ),
));
