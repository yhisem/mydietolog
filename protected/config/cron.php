<?php

// This is the configuration for yiic console application.
// Any writable CConsoleApplication properties can be configured here.
return array(
	'basePath' => dirname(__FILE__) . DIRECTORY_SEPARATOR . '..',
	'name' => 'Cron - LifeGid.com',
	'timeZone'=>'Europe/Kiev',
	'preload' => array('log'),
	'import' => array(
		'application.components.*',
		'application.models.*',
	),
	'components' => array(
        'db' => array(
            'connectionString' => 'mysql:host=77.120.114.139;dbname=lifegid',
            'emulatePrepare' => true,
            'username' => 'lifegidDB',
            'password' => 'zq9fhrgo',
            'charset' => 'utf8',
        ),
		'log' => array(
			'class' => 'CLogRouter',
			'routes' => array(
				array(
					'class' => 'CFileLogRoute',
					'levels' => 'error, warning, notice, strict',
					'filter' => 'CLogFilter',
				),
				array(
					'class' => 'CFileLogRoute',
					'levels' => 'info',
					'filter' => 'CLogFilter',
					'logFile' => 'info.log',
				),
                array(
                    'class' => 'CFileLogRoute',
                    'levels' => 'info',
                    'filter' => 'CLogFilter',
                    'logFile' => 'emailSender.log',
                    'categories'=>'email.*',
                ),
			),
		),
	)
);