<?php

// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');

// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.
return array(
    'components' => array(
        'urlManager' => array(
            'urlFormat' => 'path',
            'class'=>'ext.i18nTools.components.UrlManager',
            'showScriptName' => false,
            'rules' => array(
                '/site/test'=>'/site/test',
                '<language:(uk|ru|en)>/posts/update/<id:\d+>/<lang:(uk|ru|en)>'=>'/posts/update',
                '<language:(uk|ru|en)>/posts/delete'=>'/posts/delete',
                '<language:(uk|ru|en)>/posts/view/<id:\d+>'=>'/posts/view',
                '<language:(uk|ru|en)>/posts/index'=>'/posts/index',
                '<language:(uk|ru|en)>/posts/create/<lang:(uk|ru|en)>/<id:\d+>'=>'/posts/create',
                '<language:(uk|ru|en)>/posts/create'=>'/posts/create',

                '<language:(uk|ru|en)>/infopage/update/<id:\d+>/<lang:(uk|ru|en)>'=>'/infopage/update',
                '<language:(uk|ru|en)>/infopage/delete'=>'/infopage/delete',
                '<language:(uk|ru|en)>/infopage/view/<id:\d+>'=>'/infopage/view',
                '<language:(uk|ru|en)>/infopage/index'=>'/infopage/index',
                '<language:(uk|ru|en)>/infopage/create/<lang:(uk|ru|en)>/<id:\d+>'=>'/infopage/create',
                '<language:(uk|ru|en)>/infopage/create'=>'/infopage/create',

                '<language:(uk|ru|en)>/category/update/<id:\d+>/<lang:(uk|ru|en)>'=>'/category/update',
                '<language:(uk|ru|en)>/category/create/<lang:(uk|ru|en)>/<id:\d+>'=>'/category/create',
                '<language:(uk|ru|en)>/category/create'=>'/category/create',

                '<language:(uk|ru|en)>'=>'site/index',
                ''=>'site/index',
                '<language:(uk|ru|en)>/<controller:\w+>/<id:\d+>' => '<controller>/views',
                '<language:(uk|ru|en)>/<controller:\w+>/<action:\w+>/<id:\d+>' => '<controller>/<action>',
                '<language:(uk|ru|en)>/<controller:\w+>/<action:\w+>' => '<controller>/<action>',
                '<language:(uk|ru|en)>/<module:\w+>/<controller:\w+>/<action:\w+>' => '<module>/<controller>/<action>',
            ),
        ),
    ),
);
