<?php
return CMap::mergeArray(require('main.php'), array(
	'components' => array(
        'db'=>array(

//            'connectionString' => 'mysql:host=185.28.20.10;dbname=u806300599_kuiv',
//            'username' => 'u806300599_kuiv',
//            'password' => 'fynjy7fynjy7',
//
            'connectionString' => 'mysql:host=93.190.46.6;dbname=uh301769_diet',
            'username' => 'uh301769_diet',
            'password' => 'mydietolog',

            'emulatePrepare' => true,
            'charset' => 'utf8',
        ),
		'log' => array(
			'class' => 'CLogRouter',
			'routes' => array(
				array(
					'class' => 'CFileLogRoute',
					'levels' => 'error, warning, notice, strict',
					'filter'=>'CLogFilter',
				),
				array(
					'class' => 'CFileLogRoute',
					'levels' => 'info',
					'filter'=>'CLogFilter',
					'logFile'=>'info.log',
				),
                array(
                    'class' => 'CFileLogRoute',
                    'levels' => 'info',
                    'filter' => 'CLogFilter',
                    'logFile' => 'mainInfo.log',
                    'categories'=>'main.*',
                ),
                array(
                    'class' => 'CFileLogRoute',
                    'levels' => 'info',
                    'filter' => 'CLogFilter',
                    'logFile' => 'access.log',
                    'categories'=>'access.*',
                ),

			),
		),
//        'cache' => array(
//            'class' => 'CMemCache',
//            'useMemcached'=>true,
//            'keyPrefix'=>'LG.',
//            'servers' => array(
//                array(
//                    'host' => '127.0.0.1',
//                    'port' => 11211,
//                    'weight' => 1024,
//                ),
//            ),
//        ),
	)
));