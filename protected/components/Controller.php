<?php

class Controller extends CController
{
	public $layout='//layouts/simple';
	public $menu=array();
	public $breadcrumbs=array();

    public $pageDescription = '';
    public $pageKeywords = '';
    public $pageTitle = '';



    public function beforeRender($view)
    {
        if (!empty($this->pageTitle))
            $this->pageTitle.= ' - MyDietolog';
        return true;
    }

    public function behaviors()
    {
        return array(
            'i18n' => array(
                'class' => 'ext.i18nTools.behaviors.I18nControllerBehavior',
            ),
        );
    }

    public function __construct($id, $module = null)
    {
        parent::__construct($id, $module);
//        if (isset($_GET['ref']))
//            $_COOKIE['referrer']=$_GET['ref'];
    }

}