<?php
/**
 * Created by Sultan
 * User: ert
 * Date: 3/18/13
 * Time: 5:34 PM
 * To change this template use File | Settings | File Templates.
 */
class CStateRegexAuthManager extends CApplicationComponent implements IAuthManager
{
    public $regex = null;

    public function getRegex()
    {
        $regexes = Yii::app()->user->getState("auth-regexes");

        $id = Yii::app()->user->getId();

	        if (!is_array($regexes) || YII_DEBUG )
	        {
		        $cmd = Yii::app()->db->createCommand("SELECT DISTINCT(object) FROM(
                SELECT * FROM AccessUser AU WHERE AU.user=:user
                UNION
                SELECT * FROM AccessGroup AG WHERE AG.group IN (SELECT UGU.group FROM UserGroupUser UGU WHERE UGU.user=:user)
              )  T");

            $regexes = $cmd->queryColumn(array(":user" => $id));

            if (!count($regexes)) $regexes = array( '0' => 'no-access' );

            Yii::app()->user->setState("auth-regexes", $regexes);
        }

        if ( YII_DEBUG ) Yii::log("Allowed access objects: ".implode(",", $regexes),'info','access');

        $this->regex = "^((".implode(')|(', $regexes)."))$";

        return $this->regex;
    }

    public function checkAccess($itemName,$userId,$params=array())
    {
        $id = Yii::app()->user->getId();
        if ($userId !== $id)
            return false;

        if (preg_match("/".$this->getRegex()."/", $itemName))
            return true;
        else
            return false;
    }

    public function createAuthItem($name, $type, $description = '', $bizRule = null, $data = null)
    {
        // TODO: Implement createAuthItem() method.
    }

    public function removeAuthItem($name)
    {
        // TODO: Implement removeAuthItem() method.
    }

    public function getAuthItems($type = null, $userId = null)
    {
        // TODO: Implement getAuthItems() method.
    }

    public function getAuthItem($name)
    {
        // TODO: Implement getAuthItem() method.
    }

    public function saveAuthItem($item, $oldName = null)
    {
        // TODO: Implement saveAuthItem() method.
    }

    public function addItemChild($itemName, $childName)
    {
        // TODO: Implement addItemChild() method.
    }

    public function removeItemChild($itemName, $childName)
    {
        // TODO: Implement removeItemChild() method.
    }

    public function hasItemChild($itemName, $childName)
    {
        // TODO: Implement hasItemChild() method.
    }

    public function getItemChildren($itemName)
    {
        // TODO: Implement getItemChildren() method.
    }

    public function assign($itemName, $userId, $bizRule = null, $data = null)
    {
        // TODO: Implement assign() method.
    }

    public function revoke($itemName, $userId)
    {
        // TODO: Implement revoke() method.
    }

    public function isAssigned($itemName, $userId)
    {
        // TODO: Implement isAssigned() method.
    }

    public function getAuthAssignment($itemName, $userId)
    {
        // TODO: Implement getAuthAssignment() method.
    }

    public function getAuthAssignments($userId)
    {
        // TODO: Implement getAuthAssignments() method.
    }

    public function saveAuthAssignment($assignment)
    {
        // TODO: Implement saveAuthAssignment() method.
    }

    public function clearAll()
    {
        // TODO: Implement clearAll() method.
    }

    public function clearAuthAssignments()
    {
        // TODO: Implement clearAuthAssignments() method.
    }

    public function save()
    {
        // TODO: Implement save() method.
    }

    public function executeBizRule($bizRule, $params, $data)
    {
        // TODO: Implement executeBizRule() method.
    }
}
