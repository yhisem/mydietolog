<?php

class UserController extends Controller
{
    public $layout='//layouts/main';
    /**
     * Declares class-based actions.
     */
    public function actions()
    {

        return array(

        );
    }

    public function accessRules()
    {
        return array(
            array('allow',  // allow all users to perform 'index' and 'views' actions
                'actions'=>array('login', 'categoryList'),
                'users'=>array('*'),
            ),
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions'=>array(''),
                'users'=>array('@'),
            ),
            array('deny',  // deny all users
                'users'=>array('*'),
            ),
        );
    }

   public function actionLogin()
   {
       echo Yii::app()->user->getId();
       $loginModel = new User('login');
       if (Yii::app()->request->getPost('UserLogin')) {
           $loginModel->setAttributes(Yii::app()->request->getPost('User', array()));
           if ($loginModel->validate()) {
               $this->redirect(Yii::app()->user->returnUrl);
           }
       }
       $this->render('login', array(
           'loginModel' => $loginModel,
       ));
   }
}