<?
class AdminController extends Controller{
    public $layout='//layouts/simple';

    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'accessControl',
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        return array(
            array('allow',  // allow all users to perform 'index' and 'views' actions
                'actions'=>array('postsIndex','infopageIndex','categoryIndex','feedbackIndex','imageIndex','sliderImage', 'pictureForSliders'),
                'users'=>array('*'),
            ),
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions'=>array(''),
                'users'=>array('@'),
            ),
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions'=>array(''),
                'users'=>array('admin'),
            ),
            array('deny',  // deny all users
                'users'=>array('*'),
            ),
        );
    }

    public function actionPictureForSliders()
    {
        $model = new Images();

        if(!empty($_POST['Images'])){
            /**@var $model Images*/
            $model->save();
            $img = CUploadedFile::getInstances($model, 'extension');
            if ($img)
            {
                $imagesPath = YiiBase::getPathOfAlias('webroot') . DS . 'images' . DS . 'pictures';
                foreach ($img as $avatar)
                {
                    $imageName = $avatar->name;
                    $avatar->saveAs($imagesPath . DS . 'origin_' . $imageName);
                    $imageFile= Yii::app()->image->load($imagesPath . DS . 'origin_' . $imageName);
                    $imageFile->save($imagesPath . DS . $model->id.'.' . $imageFile->ext);
                    unlink($imagesPath . DS . 'origin_' . $imageName);
                    $model->extension = $imageFile->ext;
                    $model->save();
                }
            }
            $this->redirect(array('/admin/imageIndex'));
        }

        $this->render('create',array(
            'model'=>$model
        ));
    }

    public function actionPostsIndex()
    {
        $sqlData = " SELECT * FROM Posts P ";
        $sqlOrder = ' ORDER BY P.id DESC ';
        $sqlCount = Yii::app()->db->createCommand("SELECT count(1) FROM Posts")->queryScalar();
        $dataProvider = new CSqlDataProvider("$sqlData $sqlOrder",array(
            'keyField'=>'id',
            'totalItemCount'=>$sqlCount,
            'pagination' => array(
                'pageSize' =>6,
            ),
        ));

        $this->render('postsIndex',array(
            'dataProvider'=>$dataProvider
        ));
    }

    public function actionSliderImage()
    {
        $sqlData = " SELECT * FROM Slider S ";
        $sqlOrder = ' ORDER BY S.id DESC ';
        $sqlCount = Yii::app()->db->createCommand("SELECT count(1) FROM Slider")->queryScalar();
        $dataProvider = new CSqlDataProvider("$sqlData $sqlOrder",array(
            'keyField'=>'id',
            'totalItemCount'=>$sqlCount,
            'pagination' => array(
                'pageSize' =>6,
            ),
        ));

        $this->render('sliderImage',array(
            'dataProvider'=>$dataProvider
        ));
    }

    public function actionInfopageIndex()
    {
        $sqlData = " SELECT * FROM Infopage P ";
        $sqlOrder = ' ORDER BY P.id DESC ';
        $sqlCount = Yii::app()->db->createCommand("SELECT count(1) FROM Infopage")->queryScalar();
        $dataProvider = new CSqlDataProvider("$sqlData $sqlOrder",array(
            'keyField'=>'id',
            'totalItemCount'=>$sqlCount,
            'pagination' => array(
                'pageSize' =>6,
            ),
        ));

        $this->render('infopageIndex',array(
            'dataProvider'=>$dataProvider
        ));
    }

    public function actionCategoryIndex(){

        $sqlData = " SELECT * FROM Category C ";
        $sqlOrder = ' ORDER BY C.id DESC ';
        $sqlCount = Yii::app()->db->createCommand("SELECT count(1) FROM Category")->queryScalar();
        $dataProvider = new CSqlDataProvider("$sqlData $sqlOrder",array(
            'keyField'=>'id',
            'totalItemCount'=>$sqlCount,
            'pagination' => array(
                'pageSize' =>6,
            ),
        ));

        $this->render('categoryIndex',array(
            'dataProvider'=>$dataProvider
        ));
    }

    public function actionFeedbackIndex(){

        $sqlData = " SELECT * FROM Feedback F ";
        $sqlWhere = ' WHERE F.active = 1 ';
        $sqlOrder = ' ORDER BY F.id DESC ';
        $sqlCount = Yii::app()->db->createCommand("SELECT count(1) FROM Feedback F $sqlWhere")->queryScalar();
        $dataProvider = new CSqlDataProvider("$sqlData $sqlWhere $sqlOrder",array(
            'keyField'=>'id',
            'totalItemCount'=>$sqlCount,
            'pagination' => array(
                'pageSize' =>6,
            ),
        ));

        $this->render('feedbackIndex',array(
            'dataProvider'=>$dataProvider
        ));
}

    public function actionImageIndex(){
        $sql = " SELECT * FROM Image I ORDER BY I.id DESC ";
        $count  = Yii::app()->db->createCommand('SELECT count(1) FROM Image')->queryScalar();

        $dataProvider = new CSqlDataProvider("$sql",array(
            'keyField'=>'id',
            'totalItemCount'=>$count,
            'pagination' => array(
                'pageSize' =>6,
            ),
        ));

        $this->render('imageIndex',array(
            'dataProvider'=>$dataProvider
        ));
    }
}