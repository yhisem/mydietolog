<?php
class ImagesController extends  Controller{

    public function actionCreate(){
        $model = new Images();

        if(!empty($_POST['Images'])){
            /**@var $model Images*/
            $model->save();
            $img = CUploadedFile::getInstances($model, 'extension');
            if ($img)
            {
                $imagesPath = YiiBase::getPathOfAlias('webroot') . DS . 'images' . DS . 'pictures';
                foreach ($img as $avatar)
                {
                    $imageName = $avatar->name;
                    $avatar->saveAs($imagesPath . DS . 'origin_' . $imageName);
                    $imageFile= Yii::app()->image->load($imagesPath . DS . 'origin_' . $imageName);
                    $imageFile->save($imagesPath . DS . $model->id.'.' . $imageFile->ext);
                    unlink($imagesPath . DS . 'origin_' . $imageName);
                    $model->extension = $imageFile->ext;
                    $model->save();
                }
            }
            $this->redirect(array('/admin/imageIndex'));
        }

        $this->render('create',array(
            'model'=>$model
        ));
    }
}