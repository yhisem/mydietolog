<?php
class FeedbackController extends  Controller{

    public function actionSend(){
        if(Yii::app()->request->isAjaxRequest){
            if(isset($_REQUEST['email']) OR isset($_REQUEST['phone'])){
                $feedback = new Feedback();
                if(isset($_REQUEST['email'])) $feedback->email = $_REQUEST['email'];
                if(isset($_REQUEST['name'])) $feedback->name = $_REQUEST['name'];
                if(isset($_REQUEST['phone'])) $feedback->phone = $_REQUEST['phone'];
                if(isset($_REQUEST['text'])) $feedback->content = $_REQUEST['text'];
                $feedback->save();

                $message = new YiiMailMessage();
                $message->view = 'email_feedback';
                $message->subject = 'Обратная связь';
                $message->setBody(array('model' => $feedback), 'text/html');
                $message->setTo(array('a-gizhan@mail.ru'));
                Yii::app()->mail->send($message);

                echo json_encode(array('answer'=>true));
            }
            else
                echo json_encode(array('answer'=>false));

        }
    }

    public function actionDelete($id){
        /**@var $model Feedback*/
        $model = Feedback::model()->findByPk($id);
        $model->active = 0;
        $model->save();
        $this->redirect('/admin/feedbackIndex');
    }
}