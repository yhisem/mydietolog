<?php
class PostsController extends Controller{

    public $layout='//layouts/simple';

    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'accessControl',
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        return array(
            array('allow',  // allow all users to perform 'index' and 'views' actions
                'actions'=>array('view','index'),
                'users'=>array('*'),
            ),
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions'=>array('create','update','delete'),
                'users'=>array('@'),
            ),
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions'=>array(''),
                'users'=>array('admin'),
            ),
            array('deny',  // deny all users
                'users'=>array('*'),
            ),
        );
    }

    public function actionCreate($id = null,$lang = null)
    {
        /**
         * @var $model Posts
         */
        $model = Posts::model()->findByAttributes(array('id'=>$id,'language'=>$lang));
        if(!empty($model)){
            $this->redirect(array('update','id'=>$model->id,'lang'=>$model->language));
        }
        $model = new Posts();
        if(!empty($id) && !empty($lang)){
            $model->id = $id;
            $model->language = $lang;
        }
        else{
            $model->language = 'ru';
        }
        $model->save();
        $this->redirect(array('update','id'=>$model->id,'lang'=>$model->language));
    }

    public function actionUpdate($id,$lang)
    {
        /**
         * @var $model Posts
         */
        $this->layout = 'simple';
        $model = Posts::model()->findByAttributes(array('id'=>$id,'language'=>$lang));

        if(Yii::app()->request->isPostRequest )
        {
            $model->attributes = $_POST['Posts'];
            if($model->save())
                $this->redirect('/site/index');

        }

        $this->render('create',array(
            'model'=>$model
        ));
    }

    public function actionView($id){
        /**@var $model Posts*/

        $arrayLanguages = array('ru','uk','en');
        $lang = $_REQUEST['languages'];
        $this->layout = "simple";
        $model = Posts::model()->findByAttributes(array('id'=>$id,'language'=>$lang));

        foreach($arrayLanguages as $one){
            if($one != $lang){
                $model  = Posts::model()->findByAttributes(array('id'=>$id,'language'=>$one));
                if(!empty($model)) break;
            }
        }
//        if(empty($model)){
//            $language = (($_REQUEST['language'] == 'ru')?'uk':'ru');
//            $model  = Posts::model()->findByAttributes(array('id'=>$id,'language'=>$language));
//        }

        $this->pageDescription = $model->seoDescription;
        $this->pageKeywords = $model->seoKeywords;
        $this->pageTitle = $model->seoTitle;

        $this->render('view',array(
            'model'=>$model
        ));
    }

    public function actionIndex($categoryId = null){
        $params = array(':lang'=>$_REQUEST['language']);
        $sqlWhere = '';

        if(!empty($categoryId)){
            $sqlWhere = ' AND P.categoryId = :categoryId ';
            $params[':categoryId'] = $categoryId;
        }

        $languageIds = Yii::app()->db->createCommand("SELECT id FROM Posts P WHERE P.language = :lang $sqlWhere")->queryColumn($params);
        $languageIds = implode(',',$languageIds);
        $countLang = Yii::app()->db->createCommand(" SELECT count(*) FROM Posts P WHERE P.language = :lang $sqlWhere")->queryScalar($params);
        $newParam = array();
        $newParam[':lang'] = (($_REQUEST['language'] == 'ru')?'uk':'ru');

        $sqlP = '';$sqlPNew = '';$sqlWhereNew = ' ';
        if(!empty($languageIds)){
            $sqlP = " AND P.id not in (".$languageIds.") ";
            $sqlPNew = " AND P1.id not in (".$languageIds.") ";
        }
        if(!empty($categoryId)){
            $newParam[':categoryId'] = $categoryId;
            $sqlWhereNew = ' AND P1.categoryId = :categoryIdNew ';
            $params[':categoryIdNew'] = $categoryId;
        }

        $sqlNotLangCount = "SELECT count(*) FROM Posts P WHERE P.language = :lang $sqlP $sqlWhere ";
        $countNotLang = Yii::app()->db->
            createCommand("$sqlNotLangCount")->queryScalar($newParam);

        $category = Category::model()->findAll();

        $params[':notLang'] = (($_REQUEST['language'] == 'ru')?'uk':'ru');
        $sqlData = "SELECT * FROM(
                        SELECT * FROM Posts P WHERE P.`language` = :lang $sqlWhere
                            UNION
                        SELECT * FROM Posts P1 WHERE P1.`language` = :notLang $sqlPNew $sqlWhereNew
                    ) R ORDER BY R.id DESC";

        $dataProvider = new CSqlDataProvider("$sqlData",array(
            'keyField'=>'id',
            'totalItemCount'=>($countLang + $countNotLang),
            'pagination' => array(
                'pageSize' =>3,
            ),
            'params'=>$params
        ));

        $this->render('index',array(
            'dataProvider'=>$dataProvider,
            'category'=>$category
        ));
    }

    public function actionDelete($id,$lang){
        $model = Posts::model()->findByAttributes(array('id'=>$id,'language'=>$lang));
        $model->delete();
        $this->redirect('/admin/postsIndex');
    }
}