<?php
class CategoryController extends Controller{

    public $layout='//layouts/simple';

    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'accessControl',
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        return array(
            array('allow',  // allow all users to perform 'index' and 'views' actions
                'actions'=>array('view','index'),
                'users'=>array('*'),
            ),
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions'=>array('create','update'),
                'users'=>array('@'),
            ),
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions'=>array(''),
                'users'=>array('admin'),
            ),
            array('deny',  // deny all users
                'users'=>array('*'),
            ),
        );
    }


    public function actionCreate($id = null,$lang = null)
    {
        /**
         * @var $model Category
         */
        $model = Category::model()->findByAttributes(array('id'=>$id,'language'=>$lang));
        if(!empty($model)){
            $this->redirect(array('update','id'=>$model->id,'lang'=>$model->language));
        }
        $model = new Category();
        if(!empty($id) && !empty($lang)){
            $model->id = $id;
            $model->language = $lang;
        }
        else{
            $model->language = 'ru';
        }
        $model->save();
        $this->redirect(array('update','id'=>$model->id,'lang'=>$model->language));
    }

    public function actionUpdate($id,$lang)
    {
        /**
         * @var $model Category
         */
        $this->layout = 'simple';
        $model = Category::model()->findByAttributes(array('id'=>$id,'language'=>$lang));

        if(Yii::app()->request->isPostRequest )
        {
            $model->attributes = $_POST['Category'];
            if($model->save())
                $this->redirect('/site/index');
        }

        $this->render('create',array(
            'model'=>$model
        ));
    }

    public function actionDelete($id,$lang){
        $model = Category::model()->findByAttributes(array('id'=>$id,'language'=>$lang));
        $model->delete();
        $this->redirect('/admin/categoryIndex');
    }

}