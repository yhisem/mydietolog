<?php
class SliderImageController extends  Controller{

    public function actionCreate(){
        $model = new Images();

        if(!empty($_POST['Images'])){
            $img = CUploadedFile::getInstances($model, 'extension');
            $extension = '';
            $ids = $_POST['Images']['ids'];
            if ($img)
            {
                $imagesPath = YiiBase::getPathOfAlias('webroot') . DS . 'images' . DS . 'slider';
                foreach ($img as $avatar)
                {
                    $imageName = $avatar->name;
                    $avatar->saveAs($imagesPath . DS . 'origin_' . $imageName);
                    $imageFile= Yii::app()->image->load($imagesPath . DS . 'origin_' . $imageName);
                    $imageFile->save($imagesPath . DS . $ids.'.' . $imageFile->ext);
                    unlink($imagesPath . DS . 'origin_' . $imageName);
                    $extension = $imageFile->ext;
                }
            }

            $sliders = Slider::model()->findAllByAttributes(array('place'=>$ids,'parent'=>0));
            foreach($sliders as $slider){
                $slider->ext = $extension;
                $slider->save();
            }

            $this->redirect(array('/admin/sliderImage'));
        }

        $this->render('create',array(
            'model'=>$model
        ));
    }

    public function actionUpdate($id){
        $slider = Slider::model()->findByPk($id);

        if(!empty($_POST['Slider'])){
            $slider->attributes = $_POST['Slider'];
            $slider->save();
            $this->redirect('/admin/sliderImage');
        }

        $this->render('update',array(
            'model'=>$slider
        ));
    }
}