<?
class InfopageController extends Controller{

    public function filters()
    {
        return array(
            'accessControl',
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        return array(
            array('allow',  // allow all users to perform 'index' and 'views' actions
                'actions'=>array('contact','about','view'),
                'users'=>array('*'),
            ),
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions'=>array('create','update','delete'),
                'users'=>array('@'),
            ),
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions'=>array(''),
                'users'=>array('admin'),
            ),
            array('deny',  // deny all users
                'users'=>array('*'),
            ),
        );
    }

    public function actionContact(){
        $this->layout = 'simple';
        $this->render('contact',array(

        ));
    }

    public function actionAbout()
    {
        $this->layout = 'main';
        $this->render('about');
    }

    public function actionCreate($id = null,$lang = null)
    {
        /**
         * @var $model Infopage
         */
        $model = Infopage::model()->findByAttributes(array('id'=>$id,'language'=>$lang));
        if(!empty($model)){
            $this->redirect(array('update','id'=>$model->id,'lang'=>$model->language));
        }
        $model = new Infopage();
        if(!empty($id) && !empty($lang)){
            $model->id = $id;
            $model->language = $lang;
        }
        else{
            $model->language = 'ru';
        }
        $model->save();
        $this->redirect(array('update','id'=>$model->id,'lang'=>$model->language));
    }

    public function actionView($id){

        /**@var $model Infopage*/
        $arrayLanguages = array('ru','uk','en');
        $lang = $_REQUEST['languages'];

        $this->layout = "simple";
        $model = Infopage::model()->findByAttributes(array('id'=>$id,'language'=>$_REQUEST['language']));
//        if(empty($model)){
//            $language = (($_REQUEST['language'] == 'ru')?'uk':'ru');
//            $model  = Infopage::model()->findByAttributes(array('id'=>$id,'language'=>$language));
//        }
        foreach($arrayLanguages as $one){
            if($one != $lang){
            $model  = Infopage::model()->findByAttributes(array('id'=>$id,'language'=>$one));
                if(!empty($model)) break;
            }
        }

        $this->pageDescription = $model->seoDescription;
        $this->pageKeywords = $model->seoKeywords;
        $this->pageTitle = $model->seoTitle;

        $this->render('view',array(
            'model'=>$model
        ));
    }

    public function actionUpdate($id,$lang)
    {
        /**
         * @var $model Infopage
         */
        $this->layout = 'simple';
        $model = Infopage::model()->findByAttributes(array('id'=>$id,'language'=>$lang));

        if(Yii::app()->request->isPostRequest )
        {
            $model->attributes = $_POST['Infopage'];
            if($model->save())
                $this->redirect('/site/index');

        }

        $this->render('create',array(
            'model'=>$model
        ));
    }

    public function actionDelete($id,$lang){
        $model = Infopage::model()->findByAttributes(array('id'=>$id,'language'=>$lang));
        $model->delete();
        $this->redirect('/admin/infopageIndex');
    }
}