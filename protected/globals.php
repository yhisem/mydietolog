<?php

/**
 * This is the shortcut to Yii::app()->user.
 * @return CWebUser
 */
function user()
{
    return Yii::app()->getUser();
}

/**
 * This is the shortcut to Yii::app()->createUrl()
 */
function url($route,$params=array(),$ampersand='&')
{
    return Yii::app()->createUrl($route,$params,$ampersand);
}

/**
 * This is the shortcut to Yii::t() with default category = 'stay'
 */
function t($category,$message, $params = array(), $source = null, $language = null)
{
    return Yii::t($category, $message, $params, $source, $language);
}

function globalPosts()
{
    /**
     * @var $user User
     */
    if(!Yii::app()->user->isGuest)
    {
        $rating = Yii::app()->user->getState('userRating');
        if( $rating >= 0){
            return true;
        }
        elseif(empty($rating))
        {
            $user = User::model()->findByPk(Yii::app()->user->getId());
            Yii::app()->user->setState('userRating',$user->rating);
            globalPosts();
        }
        elseif($rating < 0)
        {
            return false;
        }
    }
    else
        return false;
}

/**
 * usage example: issetdef($_REQUEST['param'], 'default value', array('possible value 1', 'possible value 2'))
 *
 * @param $var
 * @param $def
 * @param $possible
 * @return
 */
function issetdef(&$var, $def=null, $possible=null)
{
    if (isset($var))
    {
        if ($possible===null)
            return $var;
        if (is_array($possible))
            return in_array($var, $possible) ? $var : $def;
        if ($var == $possible)
            return $var;
    }
    return $def;
}

function status($data,$id = null)
{
    $res= '';
    switch($data){
        case 'approved' :
            $res = CHtml::link('<img src="/images/icons/tick.png">Утверждено ',array('posts/changeViews','id'=>$id));
            break;
        case 'error' :
            $res =  CHtml::link('<img src="/images/icons/tick-red.png">Забракоовано',array('posts/changeViews','id'=>$id));
            break;
        case 'rejected' :
            $res =  CHtml::link('<img src="/images/icons/tick-red.png">Забракоовано',array('posts/changeViews','id'=>$id));
            break;
        case 'create' :
            $res =  CHtml::link('<img src="/images/icons/document--pencil.png">Создано',array('posts/changeViews','id'=>$id));
            break;
        case 'draft' :
            $res = CHtml::link('<img src="/images/icons/lifegid.png" >Черновик',array('posts/changeViews','id'=>$id));
            break;
    }
    return $res;
}

function getId(){
    $id = Yii::app()->user->isGuest();
    if(empty($id))
        $id = 0;
    return $id;
}



/**
 * usage example: nemptydef($_REQUEST['param'], 'default value', array('possible value 1', 'possible value 2'))
 * @param $var
 * @param $def
 * @param $possible
 * @return
 */
function nemptydef(&$var, $def=null, $possible=null)
{
    if (!empty($var))
    {
        if ($possible===null)
            return $var;
        if (is_array($possible))
            return in_array($var, $possible) ? $var : $def;
        if ($var == $possible)
            return $var;
    }
    return $def;
}