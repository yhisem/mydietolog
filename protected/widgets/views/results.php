<article class="grid_8 bcolor-1" style="height: 384px;margin: 5px 0;">
    <div class="indent">
        <h4 class="border-bot3" style="font-size: 32px;margin: 0;">Наши достижения</h4>
        <div id="type-postcard-4">
            <div>
                <p class="color-2" style="text-align: center; font-size: 20px;">Слободянюк Вячеслав</p>
                <img src="../../../images/PfotoForSlider/1slide.png" style="height:134px;width: 134px;margin: 0 auto 10px;display: block; border-radius: 100px;">
                <p class="color-2" style="font-size: 34px; text-align: center;">-200 кг</p>
                <p class="color-2" style="font-size: 18px; text-align: center;">за 8 месяцев</p>
            </div>
            <div>
                <p class="color-2" style="text-align: center; font-size: 20px;">Слободянюк Вячеслав</p>
                <img src="../../../images/PfotoForSlider/2slide.png" style="height:134px;width: 134px;margin: 0 auto 10px;display: block; border-radius: 100px;-webkit-border-radius: 100px;-moz-border-radius: 100px;">
                <p class="color-2" style="font-size: 34px; text-align: center;">-200 кг</p>
                <p class="color-2" style="font-size: 18px; text-align: center;">за 8 месяцев</p>
            </div>
            <div>
                <p class="color-2" style="text-align: center; font-size: 20px;">Слободянюк Вячеслав</p>
                <img src="../../../images/PfotoForSlider/3slide.png" style="height:134px;width: 134px;margin: 0 auto 10px;display: block; border-radius: 100px;">
                <p class="color-2" style="font-size: 34px; text-align: center;">-200 кг</p>
                <p class="color-2" style="font-size: 18px; text-align: center;">за 8 месяцев</p>
            </div>
        </div>
    </div>
</article>
<script type="text/javascript" src="/js/simpleSlider.js"></script>
<script>
    $(document).ready(function()
    {
        $('#type-postcard-4').simpleSlider({width:266,height:270,autoStart:true});
    });
</script>