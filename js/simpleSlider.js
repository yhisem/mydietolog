/**
 * Created by Yon on 04.04.14.
 */
jQuery.fn.simpleSlider = function (options)
{
    var $this=$(this);
    options = jQuery.extend({
        name:$this.attr('id'),
        width:543,
        height:400,
        autoStart: false
    }, options);

    return this.each(function ()
    {
        var $this=$(this);
        var $name=$this.attr('id');
        var count=$this.children().length;
        $this.addClass("titSlider");
        $this.children().addClass("titSlider-item");
        $this.children().css("width",options.width+"px")
        $this.children().css("height",options.height-35+"px")
        $this.css("height",options.height+"px")
        $this.css("width",options.width+"px")
        $this.css("overflow","hidden");
        if (count>0)
            $this.children().first().addClass("titSlider-active");

        var controls = $("<div class='titSlider-controls'></div>");
        var btnPrev = $("<div class='titSlider-prev'>Prev</div>");
        var state = $("<div class='titSlider-state'></div>");
        var btnNext = $("<div class='titSlider-next'>Next</div>");
        controls.append(btnPrev);
        controls.append(state);
        controls.append(btnNext);
        $this.prepend(controls);
        state.html("1 / "+count);

        function transition(from, to)
        {
//            from.removeClass("titSlider-active");

            from.fadeOut(500,function(){
                from.css("display",null);
                from.removeClass("titSlider-active")
            });
//            to.addClass("titSlider-active").fadeIn(500);
            to.fadeIn(500, function(){
                to.addClass("titSlider-active");
            });

            state.html(to.prevAll(".titSlider-item").length+1+" / "+count);
        }

        btnNext.click(function(){
            var active = $this.find(".titSlider-active");
            if (!active.length)
                active = $this.find(".titSlider-item").first();
            var next = active.next(".titSlider-item");
            if (!next.length)
                next = $this.find(".titSlider-item").first();
            transition(active, next);
        });

        btnPrev.click(function(){
            var active = $this.find(".titSlider-active");
            if (!active.length)
                active = $this.find(".titSlider-item").first();
            var prev = active.prev(".titSlider-item");
            if (!prev.length)
                prev = $this.find(".titSlider-item").last();
            transition(active, prev);
        });

        if(options.autoStart)
        {
            autoStart();
        };

        function autoStart()
        {
            btnNext.click();
            setTimeout(autoStart, 4000);
        };
    });

};
